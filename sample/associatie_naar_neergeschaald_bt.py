"""
Script om neergeschaalde beheertypen toe te kennen aan plantassociaties, op basis van relatie associatie met
parent-beheertype.
Hans Roelofsen, WEnR november 2020

"""


import pandas as pd
from sample.mnp import helpers as hlp

asso = pd.read_excel(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\MNP\draakgracht\plant\vertaling_IPO_ASSO_2009_ps_copyHDR.xls',
                     sheet_name='vertaling_IPO_ASSO_2009')
asso.loc[:, 'bt'] = asso.Bc.apply(hlp.fix_bt, as_mnp=True)
bt = hlp.BT20190612()
btcode2btomschrijving = dict(zip(bt.units.nbt, bt.units.desc))
nbts = bt.units.loc[bt.units.neergeschaald == 'ja', 'nbt']
parents = nbts.str.slice(start=0, stop=6).apply(hlp.fix_bt, as_mnp=True)
nbt2parent = dict(zip(nbts, parents))

holder = []
for nbt, parent in nbt2parent.items():
    sel = asso.loc[asso.bt == parent, :]
    if sel.empty:
        print('fail for parent-nbt {0} {1}'.format(parent, nbt))
    else:
        sel.loc[:, 'Bc'] = nbt
        sel.loc[:, 'Bn'] = btcode2btomschrijving[nbt]
        holder.append(sel)

asso_updated = pd.concat(holder)
if set(nbts).issubset(set(asso_updated.Bc)):
    asso_updated.drop(columns=['bt']).to_csv(r'c:/apps/z_temp/fuck.csv', index=False, header=True, sep=';')

print(asso_updated.head(10))






