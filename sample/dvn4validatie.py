""""
Script om de De Vegetatie van Nederland (DVN) gegevens geschikt te maken voor gebruik in autom. MNP validatie

Input formatering:
vegNED2020_header.csv  (plot header data)
vegNED2020_species.csv (plot species inventory)

Gewenste formatering
Species_Code, Uurhok_ID, COUNT
Sxxxxx, xxxx_yyyy, q

door: Hans Roelofsen, november 2020
"""

import numpy as np
import pandas as pd
from sample.mnp import helpers as hlp

headers_src = r'\\wur\dfs-root\PROJECTS\QMAR\MNP-SNL-Validation2019\TUrboveg data\vegNED2020_header.csv'
species_src = r'\\wur\dfs-root\PROJECTS\QMAR\MNP-SNL-Validation2019\TUrboveg data\vegNED2020_species.csv'

# read plot inventories
species = pd.read_csv(species_src, sep='\t', usecols=['PlotObservationID', 'Species name'])
species.rename(mapper={'Species name': 'Scientific_name'}, inplace=True, axis=1)

# read dvn headers and add uurhok identification
headers = pd.read_csv(headers_src, sep='\t',
                      usecols=['PlotObservationID', 'PlotID', 'Datum opname', 'X-Coordinaat (m)', 'Y-Coordinaat (m)'])
headers.set_index(keys='PlotObservationID', drop=False, verify_integrity=True)
headers.rename(mapper={'X-Coordinaat (m)': 'x', 'Y-Coordinaat (m)': 'y'}, inplace=True, axis=1)
headers.dropna(subset=['x', 'y'], inplace=True)
headers['coord'] = list(zip(headers.x, headers.y))
headers['uh_id'] = headers.coord.apply(hlp.pnt2square_id, size=5000, of='brief')

# Intersection between two datasets
plot_id_intersect = headers.index.intersection(set(species.PlotObservationID))
headers = headers.loc[plot_id_intersect]
species = species.loc[species.PlotObservationID.isin(list(plot_id_intersect))]

# Read MNP Species database for overview of all plant species
plant_params = pd.read_csv(r'W:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v07\01_MNP_versie2_Base_species_name.csv')
plants = list(set(plant_params.loc[plant_params.Species_code.str.startswith('S09'), 'Scientific_name']))
sp2code = dict(zip(plant_params.Scientific_name, plant_params.Species_code))

with open(r'c:/apps/z_temp/mnp_val/DVN4MNPVal_fails.csv', 'w') as k:
    with open(r'c:/apps/z_temp/mnp_val/DVN4MNPVal.csv', 'w') as f:
        for i, plant in enumerate(plants, start=1):
            print('Doing {0} ({1}/{2})'.format(plant, i, len(plants)))

            plot_ids = set(species.loc[species.Scientific_name == plant, 'PlotObservationID'])
            sp2plot = pd.pivot_table(data=headers.loc[plot_ids], index='uh_id', values='PlotObservationID', aggfunc='count') \
                        .rename({'PlotObservationID': 'Count'}, axis=1)
            if sp2plot.empty:
                k.write('no data remaining for {}\n'.format(plant))
            else:
                out = sp2plot.assign(Species_code=sp2code[plant], uh_id=sp2plot.index)
                f.write(out.astype({'uh_id': np.uint32}).to_csv(sep=',', index=False, header=False if i > 1 else True))

