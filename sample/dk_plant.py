"""
Draagkracht calculations for plant species based on DVN vegetatieopnames.

Target format
<species_code>,<land_type_code>,<land_type_quality>

Species codes and set are sourced from MNP parameter files.
Relation between species presence and vegetation types is sourced from DVN data, w thanks to Stephan Hennekes

Dit script staat volledig LOS van dk_vogel_vlinder.py en maakt ook GEEN gebruik van de Species en BT klasses in mnp/helpers.py. Het
zou netter zijn als dit wel gebeurt, maar daar kom ik nu (29-Oct-2020 10:39) niet meer aan toe.

Hans Roelofsen, 23 June 2020
"""

import os
import numpy as np
import itertools
import pandas as pd
import sys
import argparse

# from sample.mnp import helpers as hlp
from mnp import helpers as hlp

sys.path.append(r'c:/apps/proj_code/benb_utils')
from draagkracht_ww import draagkracht_ww as dk_ww
from fix_bt import fix_bt

parser = argparse.ArgumentParser()
parser.add_argument('mul_factor', default=5, help='multiplication factor in WW eq', type=int)
parser.add_argument('trouw_th', default=5, help='treshold for Trouw-graad', type=float)
parser.add_argument('freq_th', default=5, help='treshold for Frequentie-graad', type=float)
parser.add_argument('dk_th', default=0.1, help='minimum draagkracht', type=float)
parser.add_argument('out_dir', help='out_dir')
args = parser.parse_args()

"""output"""
out_dir = args.out_dir

"""Source data"""
plant_dir = r'W:\PROJECTS\MNP2020\MNP_2020\c_fases\f7_draagkracht\a_source_data\plant'
mnp_params_dir = r'w:\PROJECTS\qmar\MNP-SNL-ParameterSet\Parameters_v06_2019_12_09'

# TODO: deze variabele namen zijn nu nog vetonduidelijk!
src_mnp_sp_tax = r'07_MNP_versie4_par_population_factors.csv'  # species-codes and Taxonomic group
src_mnp_sp_nms = r'09_MNP_versie4_Group_Species_valid model_468.csv'  #species code, local name, scientific name
src_mnp_dk = r'03_MNP_versie6_par_density_factors_BT2019_v2.csv'  # current MNP Draagkrachten
src_assoXbts = r'vertaling_IPO_ASSO_2009_ps.xls'  # Vertaling Associatie --> Beheertype
src_assoXsps = r'q_syntaxa_soorten_frequentie_trouwsoorten.xlsx'  # Trouwgraad & frequentie soort/associatie
nbt = hlp.BT20190612()  # Alle info over de beheertypenkaart

# Merge MNP files with species information. Verify species codes and names occur once and once only in the dataset.
mnp_sp = pd.merge(left=pd.read_csv(os.path.join(mnp_params_dir, src_mnp_sp_nms), sep=','),
                  right=pd.read_csv(os.path.join(mnp_params_dir, src_mnp_sp_tax), sep=','),
                  how='left', left_on='Species_code', right_on='Species_code')
assert all([mnp_sp.shape[0] == len(set(getattr(mnp_sp, x))) for x in ['Species_code', 'Scientific_name', 'Local_name']])
# TODO: use mnp_sp.duplicated()

# Read MNP Draagkrachten
mnp_dk = pd.read_csv(os.path.join(mnp_params_dir, src_mnp_dk), sep=',',
                     usecols=['Species_code', 'Land_type_code', 'Land_type_quality'])

# Read Source data on plants
assoXbts = pd.read_excel(os.path.join(plant_dir, src_assoXbts), sheet_name='vertaling_IPO_ASSO_2009')
assoXsps = pd.read_excel(os.path.join(plant_dir, src_assoXsps), sheet_name='q_soorten_verbonden_Wieger')

'''
Reduce sub-associations: 00XX11a --> 00XX11 
Repair Beheertype codes in Input sheet vertaling_IPO_ASSO_2009.txt 04.02 --> N04.02.00
'''
assoXbts.loc[:, 'syntaxon_gen'] = assoXbts.short.str.slice(stop=6).str.upper()
assoXbts.loc[:, 'btC'] = assoXbts.Bc.apply(fix_bt, as_mnp=True)

'''Reduce sub-associations in Input sheet q_syntaxa_soorten_frequentie_trouwsoorten.txt'''
assoXsps.loc[:, 'syntaxon_gen'] = assoXsps.Syntaxon_code.str.slice(stop=6).str.upper()

'''Mappings between i) Associaties and Beheertypen (n:n) ii) name and species code'''
beheertypen = list(set(nbt.units.nbt))
bt2asso = dict.fromkeys(beheertypen, [])
for bt in beheertypen:
    bt2asso[bt] = list(set(assoXbts.loc[assoXbts.btC == bt, 'syntaxon_gen']))
scientifcname2code = dict(zip(mnp_sp.Scientific_name, mnp_sp.Species_code))
code2scientificname = dict(zip(mnp_sp.Species_code, mnp_sp.Scientific_name))
code2localname = dict(zip(mnp_sp.Species_code, mnp_sp.Local_name))

'''New dataframe with all species <-> Beheertype combinations.'''
dk = pd.DataFrame(columns=['Scientific_name', 'Land_type_code'],
                  data=[(a, b) for (a, b) in itertools.product(mnp_sp.loc[mnp_sp.Taxon_group == 'P', 'Scientific_name'],
                                                               set(nbt.units.nbt))])
dk = pd.concat([dk, pd.DataFrame(columns=['mean_frequentie', 'mean_trouw', 'Land_type_quality'])], sort=True)
dk.loc[:, 'Species_code'] = dk.Scientific_name.map(scientifcname2code)

'''Calculate draagkracht for each BT <> Species combination as function of Frequentie en Trouw'''
for row in dk.itertuples():

    # Land_type_quality == draagkracht == dk == functie Wieger Wamelink == f(trouw, frequentie) ==
    # np.min([np.multiply(np.divide(np.max([frequentie, trouw]), 100), 5), 1])

    sel = assoXsps.loc[(assoXsps.Soortnaam == row.Scientific_name) &
                       (assoXsps.syntaxon_gen.isin(bt2asso[row.Land_type_code])) &
                       (assoXsps.Frequentie >= args.freq_th) &
                       (assoXsps.Trouw >= args.trouw_th), :]

    if sel.empty:
        print('{0} X {1}: failed'.format(row.Scientific_name, row.Land_type_code))
        mean_freq, mean_trouw, draagkracht_ww = np.nan, np.nan, np.nan

    else:
        mean_freq = sel.Frequentie.mean()
        mean_trouw = sel.Trouw.mean()
        draagkracht_ww = dk_ww(frequentie=mean_freq, trouw=mean_trouw, th=args.dk_th, tuning=args.mul_factor)
        print('{0} X {1}: n={2} freq={3} trouw={4} dk={5}'
              .format(row.Scientific_name, row.Land_type_code, sel.shape[0], mean_freq, mean_trouw, draagkracht_ww))

    dk.loc[row.Index, 'mean_frequentie'] = mean_freq
    dk.loc[row.Index, 'mean_trouw'] = mean_trouw
    dk.loc[row.Index, 'Land_type_quality'] = draagkracht_ww

'''Write draagkracht file per species'''
fails = []
goods = []
for species_code in list(set(dk.Species_code)):
    local_name = code2localname[species_code].replace(' ', '_')
    sel = dk.loc[(dk.Species_code == species_code) & (dk.Land_type_quality.notna()),
           ['Species_code', 'Land_type_code', 'Land_type_quality']].round({'Land_type_quality': 3})
    if not sel.empty:
        goods.append(sel)

    else:
        sel = pd.Series({'Species_code': species_code, 'Local_name': local_name,
               'Scientific_name': code2scientificname[species_code], 'msg': 'failed'})
        fails.append(sel)
        print('Fail: {}'.format(local_name))
dk_out = pd.concat(goods)  ## TODO: dit zou niet nodig hoeven zijn. Put gelijk uit dk
dk_out.to_csv(os.path.join(out_dir, 'plant_draagkracht_factor{}.csv'.format(args.mul_factor)),
              index=False, header=True, sep=',')

if len(fails) > 0:
    fails_df = pd.concat(fails, axis=1)
    # fails_df.T.to_csv(r'c:\apps\z_temp\mnp_dk\03_MNP_versie7_par_density_factors_MISSING.csv',
    #                   index=True, header=True, sep='\t')

# TODO: rapport wegschrijven:
# aantal soorten aangeboden
# aantal beheertypen aangeboden
# aantal combinaties voldoen aan alle vereisten
# aantal combinaties wel data, voldoet niet aan vereisten
# etc etc

'''5. Reporting
sp_done = len(set(dk.loc[dk.dk_ww.notna(), 'sp_name']))
bt_done = len(set(dk.loc[dk.dk_ww.notna(), 'BT']))
sp_all = len(set(dk.sp_name))
bt_all = len(set(dk.BT))
u_bt = len(set(assoXbts.BT))  # nr of unique Beheertypen in WW's excel
u_sp = len(set(assoXsps.Soortnaam))  # nr of unique species in WW's excel
r_sp = len(set(mnp_sp.Scientific_name).intersection(set(assoXsps.Soortnaam)))
'''