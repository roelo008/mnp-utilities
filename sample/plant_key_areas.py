"""
calculation sheet for plant species key areas
hans roelofsen, maart 2022
"""

import pandas as pd
import numpy as np
import sys
import os

sys.path.append(r'c:/apps/proj_code')
from benb_utils.Classes import Species as species

# Sourcedata
dvn_dir = r'w:\PROJECTS\QMAR\MNP-SNL-Validation2019\TUrboveg data'
dvn_kop_src = r'vegNED2020_header.csv'
dvn_sps_src = r'vegNED2020_species.csv'

# Lees kopgegevens,
dvn_kop = pd.read_csv(os.path.join(dvn_dir, dvn_kop_src), sep='\t', usecols=list(range(0,12)), header=0,
                      names=['PlotObservationID', 'PlotID', 'Opnamenummer', 'Literatuur referentie', 'Bedekkingsschaal',
                             'Auteur', 'Datum opname', 'Bloknummer', 'syntaxon_schaminee', 'lengte_proefvlak_m',
                             'breedte_proefvlak_m', 'opp_proefvlak_sq_m'],
                      index_col='PlotObservationID')

# Reduceer kopgegevens tot opnames met BB bedekkingsschaal, bekend oppervlak en syntaxon!
dvn_kop = dvn_kop.loc[(dvn_kop.Bedekkingsschaal.isin(['Braun/Blanquet (B,D&S)', 'Braun/Blanquet'])) &
                      (dvn_kop.opp_proefvlak_sq_m.notna()) &
                      (dvn_kop.opp_proefvlak_sq_m > 0) &
                      (dvn_kop.syntaxon_schaminee.notna()), :]

# Lees plot inventarisaties
dvn_sps = pd.read_csv(os.path.join(dvn_dir, dvn_sps_src), sep='\t', header=0,
                      names=['PlotObservationID', 'SpeciesName', 'Layer', 'cover_perc', 'cover_code'])

# Intersection of plot observation IDs between dvn_kop and dvn_species
valid_plot_observation_ids = dvn_kop.index.intersection(dvn_sps.PlotObservationID)

# Reduce kopgegevens tot valide plot observation ids
dvn_kop = dvn_kop.loc[valid_plot_observation_ids]

# Reduce species gegevens tot valide plot observation ids
dvn_sps = dvn_sps.loc[dvn_sps.PlotObservationID.isin(valid_plot_observation_ids)]
assert dvn_sps.PlotObservationID.nunique() == len(valid_plot_observation_ids)

covercode2individuals = {'r': 1,
                         '+': 3,
                         '1': 30,
                         '2': 50,
                         '2m': 50,
                         '2a': 50,
                         '2b': 50,
                         '3': 50,
                         '4': 50,
                         '5': 50,
                         'x': 3,
                         '!!': 0}

# species codes of the 170 new species for Nsensitivity
codes = ['S09000010', 'S09000011', 'S09000017', 'S09000018', 'S09000029', 'S09000031', 'S09000050', 'S09000889',
         'S09000060', 'S09000098', 'S09000099', 'S09000100', 'S09000139', 'S09000143', 'S09002458', 'S09000155',
         'S09000163', 'S09000186', 'S09000199', 'S09000212', 'S09000228', 'S09000240', 'S09000244', 'S09000248',
         'S09000250', 'S09000260', 'S09000263', 'S09000264', 'S09000297', 'S09000322', 'S09000342', 'S09000349',
         'S09000384', 'S09000388', 'S09000884', 'S09000394', 'S09000420', 'S09000435', 'S09000436', 'S09000451',
         'S09000468', 'S09000469', 'S09000473', 'S09000485', 'S09000110', 'S09000549', 'S09000556', 'S09000557',
         'S09000576', 'S09000581', 'S09000598', 'S09000609', 'S09000625', 'S09000629', 'S09000658', 'S09000660',
         'S09000674', 'S09002343', 'S09000683', 'S09000693', 'S09000319', 'S09000443', 'S09000739', 'S09000763',
         'S09001933', 'S09000804', 'S09000823', 'S09000832', 'S09000843', 'S09000860', 'S09002160', 'S09000879',
         'S09000003', 'S09000909', 'S09000973', 'S09000938', 'S09000940', 'S09000944', 'S09000994', 'S09000996',
         'S09000999', 'S09001000', 'S09001008', 'S09001022', 'S09001023', 'S09001027', 'S09001030', 'S09002404',
         'S09001065', 'S09001080', 'S09001103', 'S09001124', 'S09001137', 'S09001146', 'S09000807', 'S09001226',
         'S09001235', 'S09001249', 'S09001255', 'S09001269', 'S09001271', 'S09001273', 'S09001275', 'S09001292',
         'S09002418', 'S09001153', 'S09001310', 'S09001325', 'S09001327', 'S09001329', 'S09001331', 'S09001346',
         'S09001355', 'S09001384', 'S09001385', 'S09001388', 'S09000082', 'S09000099', 'S09001083', 'S09001381',
         'S09001355', 'S09000621', 'S09000669', 'S09000136', 'S09000024', 'S09000232', 'S09000688', 'S09000925',
         'S09000784', 'S09001048', 'S09000447', 'S09000669', 'S09001140', 'S09000884', 'S09000198', 'S09000688',
         'S09000890', 'S09000651', 'S09000772', 'S09000103', 'S09000151', 'S09000329', 'S09000355', 'S09000237',
         'S09000687', 'S09000219', 'S09001593', 'S09001107', 'S09001398', 'S09001399', 'S09001256',
         'S09000285', 'S09000287', 'S09001233', 'S09000124', 'S09000444', 'S09000472', 'S09001431', 'S09000691',
         'S09000854', 'S09000986', 'S09001049', 'S09001050', 'S09001055', 'S09001206', 'S09000056', 'S09000786',
         'S09001295', 'S09001387', 'S09001295', 'S09000033', 'S09000103', 'S09000151', 'S09000365', 'S09000532',
         'S09000534', 'S09000896', 'S09000377']

species_lst = []
for code in codes:
    species_lst.append(species.IndividualSpecies(code))

out = pd.DataFrame(0, index=[sp.code for sp in species_lst],
                   columns=['scientific', 'local', 'n_plots_dvn', 'n_plots_w_species', 'perc_tov_dvn', 'n_syntaxa',
                            'n_plots_w_syntaxon', 'perc_tov_syntaxon_plots',
                            'plot_area_min', 'plot_area_max', 'plot_area_mean', 'plot_area_median',
                            'cover_perc_min', 'cover_perc_max', 'cover_perc_mean', 'cover_perc_median',
                            'bb_r', 'bb_+', 'bb_1', 'bb_2m', 'bb_2a', 'bb_2b', 'bb_3', 'bb_4', 'bb_5', 'bb_2',
                            'n_indiv_min', 'n_indiv_max', 'n_indiv_mean', 'n_indiv_median',
                            'indiv_per_sq_m_min', 'indiv_per_sq_m_max', 'indiv_per_sq_m_mean', 'indiv_per_sq_m_median'])
out['n_plots_dvn'] = dvn_kop.shape[0]

for sp in species_lst:

    print('doing {}'.format(sp.local), end='\r')

    # Add metadata
    out.loc[sp.code, 'scientific'] = sp.scientific
    out.loc[sp.code, 'local'] = sp.local

    # Identify all occurences of this species in the species inventory database
    species_sightings = dvn_sps.loc[dvn_sps.SpeciesName == sp.scientific, :]

    # Remove duplicate plots from species_sightings. Meaning that species is noted > once in a plot. This shouldn't be
    species_sightings.drop_duplicates(subset='PlotObservationID', keep='first', inplace=True)

    # Identify all plots containing this species
    plots_w_species = species_sightings.PlotObservationID

    # Identify all syntaxa in which this species occurs
    syntaxa = dvn_kop.loc[(dvn_kop.syntaxon_schaminee.notna()) &
                          (dvn_kop.index.isin(plots_w_species)), 'syntaxon_schaminee'].unique()

    # Identify all plots with a corresponding syntaxon
    plots_w_syntaxon = dvn_kop.loc[dvn_kop.syntaxon_schaminee.isin(syntaxa)]

    # Merge kopgegevens with selected kopgegevens
    plots = pd.merge(left=species_sightings,
                     right=dvn_kop.loc[:, ['opp_proefvlak_sq_m', 'syntaxon_schaminee']],
                     left_on='PlotObservationID', right_index=True, how='left')

    if plots.empty:
        continue

    # Plot counts
    out.loc[sp.code, 'n_plots_w_species'] = plots.shape[0]
    out.loc[sp.code, 'perc_tov_dvn'] = np.multiply(np.divide(plots.shape[0], dvn_kop.shape[0]), 100)
    out.loc[sp.code, 'n_syntaxa'] = len(syntaxa)
    out.loc[sp.code, 'n_plots_w_syntaxon'] = plots_w_syntaxon.shape[0]
    out.loc[sp.code, 'perc_tov_syntaxon_plots'] = np.multiply(np.divide(plots.shape[0], plots_w_syntaxon.shape[0]), 100)

    # Sum stats on plot areas
    out.loc[sp.code, 'plot_area_min'] = plots.opp_proefvlak_sq_m.min()
    out.loc[sp.code, 'plot_area_max'] = plots.opp_proefvlak_sq_m.max()
    out.loc[sp.code, 'plot_area_mean'] = plots.opp_proefvlak_sq_m.mean()
    out.loc[sp.code, 'plot_area_median'] = plots.opp_proefvlak_sq_m.median()

    # Sum stats on procentual coverage
    out.loc[sp.code, 'cover_perc_min'] = plots.cover_perc.min()
    out.loc[sp.code, 'cover_perc_max'] = plots.cover_perc.max()
    out.loc[sp.code, 'cover_perc_mean'] = plots.cover_perc.mean()
    out.loc[sp.code, 'cover_perc_median'] = plots.cover_perc.median()

    # Counts of cover codes
    cover_code_counts = plots.cover_code.value_counts()
    out.loc[sp.code, 'bb_+'] = getattr(cover_code_counts, '+', 0) + getattr(cover_code_counts, 'x', 0)
    out.loc[sp.code, 'bb_1'] = getattr(cover_code_counts, '1', 0)
    out.loc[sp.code, 'bb_r'] = getattr(cover_code_counts, 'r', 0)
    out.loc[sp.code, 'bb_2a'] = getattr(cover_code_counts, '2a', 0)
    out.loc[sp.code, 'bb_2b'] = getattr(cover_code_counts, '2b', 0)
    out.loc[sp.code, 'bb_2m'] = getattr(cover_code_counts, '2m', 0)
    out.loc[sp.code, 'bb_2'] = getattr(cover_code_counts, '2', 0)
    out.loc[sp.code, 'bb_3'] = getattr(cover_code_counts, '3', 0)
    out.loc[sp.code, 'bb_4'] = getattr(cover_code_counts, '4', 0)
    out.loc[sp.code, 'bb_5'] = getattr(cover_code_counts, '5', 0)

    # Map cover code to individuals
    n_individuals = plots.cover_code.map(covercode2individuals)
    out.loc[sp.code, 'n_indiv_min'] = n_individuals.min()
    out.loc[sp.code, 'n_indiv_max'] = n_individuals.max()
    out.loc[sp.code, 'n_indiv_mean'] = n_individuals.mean()
    out.loc[sp.code, 'n_indiv_median'] = n_individuals.median()

    # Calculate # individuals per sq m, summary stats
    n_individuals_per_sq_m = n_individuals / plots.opp_proefvlak_sq_m
    out.loc[sp.code, 'indiv_per_sq_m_min'] = n_individuals_per_sq_m.min()
    out.loc[sp.code, 'indiv_per_sq_m_max'] = n_individuals_per_sq_m.max()
    out.loc[sp.code, 'indiv_per_sq_m_mean'] = n_individuals_per_sq_m.mean()
    out.loc[sp.code, 'indiv_per_sq_m_median'] = n_individuals_per_sq_m.median()

out.to_clipboard(sep='\t')
