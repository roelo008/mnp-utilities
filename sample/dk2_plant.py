"""
Python tool for generating DRAAGKRACHT numbers for plant species for use in conjunction with the MNP model.

Essentially same content as dk_plant.py, but with improved UI, cleaner coding and elaborate output

Hans Roelofsen, WEnR, june 2021
"""

import os
import numpy as np
import itertools
import pandas as pd
import sys
import argparse
import datetime

sys.path.append(r'c:/apps/proj_code/benb_utils')
from draagkracht_ww import draagkracht_ww as dk_ww
from fix_bt import fix_bt
from Classes.Species import TabularSpecies

class foo:
    def __init__(self):
        foo.A = 5
        foo.th_dk = 0.01
        foo.th_f = 5
        foo.th_t = 5
        foo.sp_src = r'c:\apps\proj_code\mnp_utilities\sample\sample_data\species_list.csv'
        foo.bt_src = r'c:\apps\proj_code\mnp_utilities\sample\sample_data\snl_Ntypen.csv'
        foo.out_dir = r'c:\apps\proj_code\mnp_utilities\sample\sample_data'

args = foo()

tab_species = TabularSpecies()

def plant_dk(args):
    """
    Function to calculate draagkracht, see help below.
    :param A:
    :param th_dk:
    :param th_f:
    :param th_t:
    :param sp_src:
    :param bt_src:
    :param out_dir:
    :return: *.csv and *.txt
    """

    # Gather default data on plants
    plant_dir = r'W:\PROJECTS\MNP2020\MNP_2020\c_fases\f7_draagkracht\a_source_data\plant'
    if not os.path.isdir(plant_dir):
        raise AssertionError('Please connect to W:\\projects\MNP2020')

    # Vertaling associatie <-> beheertype. Add generalized syntaxon code and repair beheertypen ("01.02" -> "N01.02.00")
    plant_associaties_src = 'vertaling_IPO_ASSO_2009_ps.xls'  # Vertaling Associatie --> Beheertype
    associaties = pd.read_excel(os.path.join(plant_dir, plant_associaties_src), sheet_name='vertaling_IPO_ASSO_2009')
    associaties['syntaxon_gen'] = associaties.short.str.slice(stop=6).str.upper()  # Generalize sub-Ass to Associatie
    associaties['beheertype'] = associaties.Bc.apply(fix_bt, as_mnp=True, pass_missing_N=True)  # Fix beheertypen codes

    # Read plant response (Trouw/Frequentie) to Associaties, add generalized syntaxon code to plant response table
    plant_responses_src = 'q_syntaxa_soorten_frequentie_trouwsoorten.xlsx'  # Trouwgraad & frequentie soort/associatie
    responses = pd.read_excel(os.path.join(plant_dir, plant_responses_src), sheet_name='q_soorten_verbonden_Wieger')
    responses['syntaxon_gen'] = responses.Syntaxon_code.str.slice(stop=6).str.upper()

    # Get master list of beheertypen
    bt_master_src = r'c:\apps\proj_code\benb_utils\resources\snl_beheertypen.csv'
    try:
        bt_master = pd.read_csv(bt_master_src, sep=',', comment='#', quotechar='"')
        bt_master['bt_code'] = bt_master.LAND_TYPE_CODE.apply(fix_bt, as_mnp=True)
    except FileNotFoundError:
        raise AssertionError("please instal BenB repository and update local path to \\resources\snl_beheertypen.csv")
    btcode2omschrijving = dict(zip(bt_master.bt_code, bt_master.LST_NAME))

    # Get default mnp draagkracht values
    #mnp_dk_src = r'w:\PROJECTS\qmar\MNP-SNL-ParameterSet\Parameters_v06_2019_12_09\03_MNP_versie6_par_density_factors_BT2019_v2.csv'
    # bovenstaande bron voor MNP draagkrachten is niet de beste keuze, want daarin staan de gekke coderingen van Marlies & Henk (kolom
    mnp_dk_src = r'w:\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2021_04_08\03_MNP_versie5_par_density_factors_BT2021_v3.csv'
    try:
        mnp_dk = pd.read_csv(mnp_dk_src, sep=',', usecols=['Species_code', 'Land_type_code', 'Land_type_quality'])
    except FileNotFoundError:
        raise AssertionError('Please connect to w:\\projects\qmar')

    # Read list of requested beheertypen to analyse, add mnp-style code
    try:
        bts = pd.read_csv(args.bt_src, sep=',', comment='#')
        bts.rename(dict(zip(list(bts), [x.capitalize() for x in list(bts)])), axis=1, inplace=True)
        assert 'Land_type_code' in list(bts)
    except (ValueError, FileNotFoundError, AssertionError) as e:
        raise AssertionError('cannot read beheertypen source: {}. Check for column Land_type_code!'.format(e))
    bts['bt_code'] = bts.Land_type_code.apply(fix_bt, as_mnp=True)  # Add MNP-style Beheertype codes
    bts.drop_duplicates(subset='bt_code', inplace=True)  # Drop duplicates
    if set(bts.bt_code).difference((set(bt_master.bt_code))):
        raise AssertionError("Invalid Beheertypen found: {}".format(set(bts.bt_code).difference((set(bt_master.bt_code)))))

    # Map 1 beheertype to list of Associaties
    bt2asso = dict.fromkeys(bts.bt_code, [])
    for bt in bts.bt_code:
        bt2asso[bt] = list(set(associaties.loc[associaties.beheertype == bt, 'syntaxon_gen']))

    # Read list of requested species, add syntax group and assert all plants
    try:
        if args.sp_src.endswith('.csv'):  # Read as CSV
            species = pd.read_csv(args.sp_src, sep=',', usecols=['Species_code', 'Local_name', 'Scientific_name'])
        elif args.sp_src.endswith('.xlsx'):  # Read as XLSX
            species = pd.read_excel(args.sp_src, sheet_name=args.sheet,
                                    usecols=['Species_code', 'Local_name', 'Scientific_name'])
        else:
            raise AssertionError('provide either *.csv or *.xlsx, not {}'.format(args.sp_src))
    except (ValueError, FileNotFoundError) as e:
        raise AssertionError('Cannot find species list, or species list is missing "Species_code" (S09xxxxxx) '
                             '"Local_name" "Scientific_name" columns: {}'.format(e))
    species.drop_duplicates(subset='Species_code', inplace=True)
    plant_species = species.loc[species.Species_code.map(tab_species.code2taxon) == "P"].index
    # assert all(species.Species_code.apply(code2tax) == 'P'), "non plants found in requested species list."

    # species dicts
    code2scientific = dict(zip(species.Species_code, species.Scientific_name))
    code2local = dict(zip(species.Species_code, species.Local_name))

    """Loop trough alll Species/Beheertype combis. Calculate mean frequentie/trouw where possible. """
    dk_lst = []
    sp_enums = dict((j, i) for i, j in enumerate(species.loc[plant_species, 'Species_code'], start=1))
    for (sp_code, bt) in itertools.product(species.loc[plant_species, 'Species_code'], bts.bt_code):

        print('Doing {0} ({1}/{2})'.format(sp_code, sp_enums[sp_code], len(plant_species)), end="\r")

        d = {'Land_type_code': bt,
             'Species_code': sp_code,
             'hits': responses.loc[(responses.Soortnaam == code2scientific[sp_code]) &
                                   (responses.syntaxon_gen.isin(bt2asso[bt]))].shape[0],
             'hits_gte_th': 0,
             'mean_trouw': np.nan,
             'mean_freq': np.nan,
             'dk_ww': np.nan}

        # Continue with next if there are 0 hits for species/bt combination in the database
        if d['hits'] == 0:
            dk_lst.append(pd.DataFrame(d, index=pd.RangeIndex(1)))
            continue

        # Narrow selection further down to rows meeting thresholds
        sel1 = responses.loc[(responses.Soortnaam == code2scientific[sp_code]) &
                             (responses.syntaxon_gen.isin(bt2asso[bt])) &
                             (responses.Frequentie >= args.th_f) &
                             (responses.Trouw >= args.th_t), :].index
        d['hits_gte_th'] = len(sel1)

        d['mean_freq'] = responses.loc[sel1, 'Frequentie'].mean()
        d['mean_trouw'] = responses.loc[sel1, 'Trouw'].mean()
        d['dk_ww'] = dk_ww(frequentie=d['mean_freq'], trouw=d['mean_trouw'], th=args.th_dk, tuning=args.A)

        # Wrap up results and append to list
        dk_lst.append(pd.DataFrame(d, index=pd.RangeIndex(1)))

    # Overall draagkracht dataframe and set 0 to Nan
    dk = pd.concat(dk_lst)
    dk.hits = np.where(dk.hits == 0, np.nan, dk.hits)
    dk.hits_gte_th = np.where(dk.hits_gte_th == 0, np.nan, dk.hits_gte_th)

    # Add additional information
    try:
        dk['Scientific_name'] = dk.Species_code.map(code2scientific)
        dk['Local_name'] = dk.Species_code.map(code2local)
        dk['Land_type_omschrijving'] = dk.Land_type_code.map(btcode2omschrijving)
    except KeyError as e:
        raise AssertionError("{} is not recognized".format( e))

    # Merge with MNP default draagkracht values based on Species_codeLand_type_code combination
    dk = pd.merge(left=dk.assign(combi=dk.Species_code.str.cat(dk.Land_type_code)),
                  right=mnp_dk.assign(combi=mnp_dk.Species_code.str.cat(mnp_dk.Land_type_code))\
                              .drop(labels=['Species_code', 'Land_type_code'], axis=1)\
                              .rename(mapper={'Land_type_quality': 'dk_mnp'}, axis=1),
                  left_on='combi', right_on='combi', how='left').drop('combi', axis=1)

    # Report on results
    ts_full = datetime.datetime.now().strftime("%Y%m%dt%H%M%S")
    species_requested = species.shape[0]  # Aantal soorten gevraagd
    plants_requested = len(plant_species)  # Aantal plant soorten gevraagd
    bts_requested = bts.shape[0]  # Aantal BTs gevraagd
    combis_potential = np.multiply(plants_requested, bts_requested)  # potentiaal aantal draakgrachts
    combis_wo_data = dk.hits.isna().sum()  # combis without data
    combis_w_data = dk.hits.notna().sum()  # combi where data is provided
    combis_meeting_th = dk.hits_gte_th.notna().sum()  # combies where Trouw & Frequentie data meets criteria
    combis_lost = (dk.dk_ww.isna() & dk.dk_mnp.notna()).sum()  # Combi die eerst wel, nu geen Draagkracht heeft
    combis_new = (dk.dk_ww.notna() & dk.dk_mnp.isna()).sum()  # Combi die eerst niet, nu wel Draagkracht heeft
    combis_kept = (dk.dk_ww.notna() & dk.dk_mnp.notna()).sum()  # Combie die die eerst wel en nog steeds DK heeft
    combis_never = (dk.dk_ww.isna() & dk.dk_mnp.isna()).sum()  # Combie die toen niet, nu niet, nooit niet DK heeft

    # Pivot table
    piv = pd.pivot_table(dk.assign(Soort=dk.Scientific_name.str.cat(dk.Local_name, sep=" (")
                                                           .str.cat(pd.Series([")"]), na_rep=")"),
                                   BT=dk.Land_type_code.str.cat(dk.Land_type_omschrijving, sep=' (')
                                                       .str.cat(pd.Series([")"]), na_rep=")")),
                         index='Soort', columns='BT', aggfunc='mean', values='dk_ww', dropna=False).round(2)
    species_never = piv.loc[piv.count(axis=1) == 0].index
    beheertype_never = piv.count(axis=0).loc[piv.count(axis=0) == 0].index

    count_per_species = piv.count(axis=1).sort_values(ascending=False)
    count_per_bt = piv.count(axis=0).sort_values(ascending=False)

    # X soorten met Wieger Wamelink draagkrachten voor N beheertypen
    aantal_bts, aantal_soorten = np.unique(pd.pivot_table(data=dk, index='Species_code', aggfunc='count', values='dk_ww'),
                                           return_counts=True)

    dkww_df = pd.DataFrame({'n_beheertypen': aantal_bts, 'n_soorten': aantal_soorten})
    report = \
        "==RAPPORT AUTOMATISCHE DRAAGKRACHT BEPALING VOOR PLANTEN TBV MNP==\n"\
        "door:  {0}\ndatum: {1}\n\n" \
        "draagkracht formule:\n  dk(X,BT) = max({2}, min(((max(F,T)/100) * {3}), 1))\n\n" \
        "where:\n"\
        "  dk = draagkracht\n"\
        "  X  = plant species\n"\
        "  BT = beheertype\n"\
        "  F  = mean(f(X,A1)... f(X, An)) where f = Frequentie of X in Associaties A1...An related to BT and f >= {4}\n"\
        "  T  = mean(t(X,A1)... t(X, An)) where t = Trouw of X in Associaties A1...An related to BT and t >= {5}.\n\n"\
        "input soortenlijst:     {6}\n"\
        "input beheertypen:      {7}\n"\
        "referentie trouw/freq:  {8}\n"\
        "referentie associaties: {9}\n"\
        "referentie beheertypen: {10}\n"\
        "referentie mnp draagkr: {11}\n\n"\
        "n species gevraagd:     {12}\n" \
        "n planten gevraagd:     {13}\n"\
        "n beheertypen gevraagd: {14}\n"\
        "n potentiale dk combis: {15}\n"\
        "n combis w/o data:      {16}\n"\
        "n combis w data:        {17}\n"\
        "n combis w data >= th:  {18}\n"\
        "n combis new tov ref:   {19}\n"\
        "         lost       :   {20}\n"\
        "         kept       :   {21}\n"\
        "         never      :   {22}\n\n" \
        "Aantal soorten met draagkracht voor X beheertypen:\n" \
        "{23}\n" \
        "Aantal draagkrachten per soort\n"\
        "{24}\n\n"\
        "Aantal draagkrachten per beheertype\n"\
        "{25}\n\n"\
        "Draaitabel:\n"\
        "{26}".format(os.getenv('USERNAME'),  #0
                      ts_full,  #1
                      args.th_dk,  #2
                      args.A,  #3
                      args.th_f,  #4
                      args.th_t,  #5
                      args.sp_src,  #6
                      args.bt_src,  #7
                      os.path.join(plant_dir, plant_responses_src),  #8
                      os.path.join(plant_dir, plant_associaties_src),  #9
                      bt_master_src,  #10
                      mnp_dk_src, #11
                      species_requested,  #12
                      plants_requested,  #13
                      bts_requested,  #14
                      combis_potential,  #15
                      combis_wo_data,  #16
                      combis_w_data,  #17
                      combis_meeting_th,  #18
                      combis_new,  #19
                      combis_lost,  #20
                      combis_kept,  #21
                      combis_never,  #22
                      dkww_df.to_csv(index=False, sep='\t', line_terminator='\n'),  #23
                      count_per_species.to_csv(sep='\t', header=False, line_terminator='\n'),  #24
                      count_per_bt.to_csv(sep='\t', header=False, line_terminator='\n'),  #25
                      piv.to_csv(sep=',', line_terminator='\n'))  #26

    # Prepare output
    basename = '03PlantDensityFactors{0}'.format(ts_full)
    assert os.path.isdir(args.out_dir), "{} is not a valid output directory".format(args.out_dir)

    # Write report
    with open(os.path.join(args.out_dir, '{}.txt'.format(basename)), 'w') as f:
        f.write(report)

    # MNP style CSV output
    # out = dk.dropna(axis=0, subset=['dk_ww'])\
    out = dk.rename(mapper={'dk_ww': 'Land_type_quality_auto'}, axis=1)\
      .loc[:, ['Species_code', 'Land_type_code', 'Land_type_quality_auto', 'dk_mnp', 'Local_name', 'Scientific_name',
               'Land_type_omschrijving']]\
      .round({'Land_type_quality_auto': 2})
    if args.of == 'csv':
        out.to_csv(os.path.join(args.out_dir, '{}.csv'.format(basename)), sep=',', header=True, index=False)
    elif args.of == 'df':
        return out

    print('\n...done')


if __name__ == '__main__':

    def test(x):
        # Provide default values to the args Namespace
        print('Testing...')
        try:
            plant_dk(x)
        except AssertionError as e:
            print('Error: {}'.format(e))
            sys.exit(1)

    def full(x):
        print('Calculating draagkracht as specified...')
        try:
            plant_dk(x)
        except AssertionError as e:
            print('Error: {}'.format(e))
            sys.exit(1)

    model_desc = \
        'For a list of Species (X1, X2...Xn) and a list of Beheertypen (BT1, BT2...BTn), calculate draagkracht for ' \
        'each X-BT combination as:\n\n' \
        '  dk(X,BT) = max(th_dk, min(((max(F,T)/100) * A), 1))\n\n' \
        'where:\n' \
        '  F = mean(f(X,A1)... f(X, An)) where f = Frequentie of X in Associaties A1...An that are related to BT.\n' \
        '  T = mean(t(X,A1)... t(X, An)) where t = Trouw of X in Associaties A1...An that are related to BT.\n\n' \
        'type "test" for trial run with default values and sample data.\n'\
        'type "full" and provide positional and optional arguments below otherwise.\n\n'\
        'positional arguments:\n'\
        '  A                 parameter, usually between 1-5\n' \
        '  th_dk             overall treshold for draagkracht, usually 0.1\n' \
        '  th_f              treshold for f(X, Ax), usually 5\n' \
        '  th_t              treshold for t(X, Ax), usually 5\n' \
        '  sp_src            CSV list of plant species formatted Species_code, Local_name, Scientific_name\n' \
        '  bt_src            CSV list of Beheertypen with at least column Land_type_code formatted NXX.YY.ZZ or NXX.YY' \
        '  of                output format: csv to file, or return as dataframe. Default: csv'

    parser = argparse.ArgumentParser(description=model_desc, formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(dest='subparser_name', help=argparse.SUPPRESS)
    parser_test = subparsers.add_parser("test", help='test w sample data', description=model_desc,
                                        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_test.set_defaults(func=test, A=5, th_dk=0.1, th_f=5, th_t=5, sp_src=r'.\sample_data\species_list.csv',
                             bt_src=r'.\sample_data\bts.csv', out_dir=r'.\sample_data', of='csv')

    parser_full = subparsers.add_parser('full', help='run with user spec data and setting', description=model_desc,
                                        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_full.add_argument('A', help=argparse.SUPPRESS, type=int)
    parser_full.add_argument('th_dk', help=argparse.SUPPRESS, type=float)
    parser_full.add_argument('th_f', help=argparse.SUPPRESS, type=float)
    parser_full.add_argument('th_t', help=argparse.SUPPRESS, type=float)
    parser_full.add_argument('sp_src', help=argparse.SUPPRESS, type=str)
    parser_full.add_argument('bt_src', help=argparse.SUPPRESS, type=str)
    parser_full.add_argument('of', help=argparse.SUPPRESS, type=str, default='csv')
    parser_full.add_argument('--out_dir', default=r'./', help='output directory', type=str)
    parser_full.add_argument('--sheet', help='xls sheet name for sp_src', type=str)
    parser_full.set_defaults(func=full)
    fargs = parser.parse_args()
    fargs.func(fargs)


