"""
Helper functions delivering information.
"""

import os
import random
import numpy as np
import rasterio as rio
import geopandas as gp
import pandas as pd
import shapely
import affine
import numbers
import argparse
#from matplotlib import pyplot as plt
import xml.etree.ElementTree as ET


def plt_bar(labels, values, species, ha):
    x = np.arange(len(labels))

    fig = plt.figure(figsize=(14, 8))
    ax = fig.add_subplot(111)
    ax.set(xlim=[-1, len(labels) + 1], ylim=[0, 100])
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation='vertical', horizontalalignment='center')
    ax.hlines(y=[x for x in range(0, 100, 10)], xmin=-1, xmax=len(labels) + 1, colors='lightgrey')
    # ax.hlines(y=[x for x in range(0, 200, 50)], xmin=-1, xmax=len(labels) + 1, linewidths=1)
    ax.bar(x, values, label='{}, tot. opp. = {:,}ha'.format(species, ha), width=0.6, align='center')
    ax.set_ylabel('% van totale oppervlakte van NDFF waarnemingen sinds 2000')
    fig.tight_layout()
    ax.legend()
    out_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\MNP\draakgracht'
    out_name = 'ndff_bt_{0}.png'.format(species)
    fig.savefig(os.path.join(out_dir, out_name))
    print('I made a figure: {0}'.format(os.path.join(out_dir, out_name)))
    plt.close()


def read_ndff_shp(src):
    """
    Read and query an NDFF shapefile
    :param species: as helpers.species object
    :param min_year: query NDFF observations to years AFTER min_year
    :param max_area: query NDFF observations to those with area LESS THAN max_area
    :return: ndff shapefile as geopandas geodataframe with additional attributes
    """

    ndff = gp.read_file(src, encoding='UTF-8')
    ndff['nl_naam_lower'] = ndff['soort_ned'].str.lower()
    return ndff


def query_ndff(gdf, species, max_area, min_year):
    """
    Query an existing NDFF geodataframe for a species and area
    :param gdf: ndff gdf
    :param species: species object
    :param max_area: integer
    :return:
    """

    queries = {'q1': gdf.loc[(gdf.year >= min_year) & (gdf.year.notna())].index,
               'q2': gdf.loc[gdf.nl_naam_lower == species.naam_ned].index,
               'q3': gdf.loc[gdf.opp_m2 <= max_area].index}
    sel = set.intersection(*[set(queries[x]) for x in ['q1', 'q2', 'q3']])  # TODO: select queries w **kwargs
    assert len(sel) > 0, 'no observations remaining'
    print('  found {0} NDFF observations for {1}'.format(len(sel), species.name))
    return gdf.loc[sel]


def write2file(df, out_name, out_dir):
    """
    write output dataframe to file
    :param df: dataframe
    :param out_name:
    :param out_dir:
    :return: out_dir/out_name.csv
    """

    out_file = os.path.join(out_dir, '{}.csv'.format(out_name))
    df.to_csv(out_file, sep=',', header=True, index=False)
    print('  written to file: {}'.format(out_file))

def rand_web_color_hex():
    """"
    Generate random color.
    https://blog.stigok.com/2019/06/22/generate-random-color-codes-python.html
    """

    rgb = ""
    for _ in "RGB":
        i = random.randrange(0, 2**8)
        rgb += i.to_bytes(1, "big").hex()
    return '#{}'.format(rgb)


class BT20190612:
    """
    Class holding info related to neergeschaalde beheertypekaart v 20190612.
    Gerepareerde beheertypen van 2016, inclusief agrarisch, aangevuld met landschapselementen (beginnend met L of S)
    """

    def __init__(self):
        self.src = r'W:\PROJECTS\qmar\MNP-SNL-SpatialData\BT_Huidig_20190612\dominatie_25m\dominant.flt'
        self.raster = rio.open(self.src)

                        # code, omschrijving, pixel_value
        nbt_contents = [['0', 'NoData', -9999, 'nee'],
                        ["0", "Geen", 0, 'nee'],
                        ["A01.01.00", "Weidevogelgebied", 15001, 'nee'],
                        ["A01.02.00", "Akkerfaunagebied", 15002, 'nee'],
                        ["A01.03.00", "Ganzenfourageergebied", 15003, 'nee'],
                        ["A01.04.00", "Insectenrijk grasland", 15004, 'nee'],
                        ["A02.01.00", "Botanisch waardevol grasland", 15011, 'nee'],
                        ["A02.02.00", "Botanisch waardevol akkerland", 15012, 'nee'],
                        ["A11.02.00", "Weidevogelland met riet of opgaande begroeiing", 15013, 'nee'],
                        ["A12.01.00", "Open akkerland voor broedende akkervogels", 15014, 'nee'],
                        ["A12.02.00", "Open akkerland voor overwinterende akkervogels", 15015, 'nee'],
                        ["L01.01.00", "Poel en kleine historische wateren", 15121, 'nee'],
                        ["L01.02.00", "Houtwal en houtsingel", 15122, 'nee'],
                        ["L01.03.00", "Elzensingel", 15123, 'nee'],
                        ["L01.04.00", "Bossingel en bosje", 15124, 'nee'],
                        ["L01.05.00", "Knip- of scheerheg", 15125, 'nee'],
                        ["L01.06.00", "Struweelhaag", 15126, 'nee'],
                        ["L01.07.00", "Laan", 15127, 'nee'],
                        ["L01.08.00", "Knotboom", 15128, 'nee'],
                        ["L01.09.00", "Hoogstamboomgaard", 15129, 'nee'],
                        ["L01.10.00", "Struweelrand", 15130, 'nee'],
                        ["L01.11.00", "Hakhoutbosje", 15131, 'nee'],
                        ["L01.12.00", "Griendje", 15132, 'nee'],
                        ["L01.13.00", "Bomenrij of solitaire boom", 15133, 'nee'],
                        ["L01.14.00", "Rietzoom en klein rietperceel", 15134, 'nee'],
                        ["L01.15.00", "Natuurvriendelijk oever", 15135, 'nee'],
                        ["L02.01.00", "Fortterrein", 15251, 'nee'],
                        ["L02.02.00", "Historisch bouwwerk en erf", 15252, 'nee'],
                        ["L02.03.00", "Historische tuin", 15253, 'nee'],
                        ["L03.01.00", "Aardwerk en groeve", 15354, 'nee'],
                        ["L04.01.00", "Wandelpad over boerenland", 15455, 'nee'],
                        ["N00.01.00", "Nog om te vormen landbouwgrond naar natuur (inrichting)", 1, 'nee'],
                        ["N00.02.00", "Nog om te vormen natuur naar natuur (functieverandering)", 2, 'nee'],
                        ["N01.01.00", "Zee en wad", 101, 'nee'],
                        ["N01.02.00", "Duin- en kwelderlandschap", 102, 'nee'],
                        ["N01.03.00", "Rivier- en moeraslandschap", 103, 'nee'],
                        ["N01.04.00", "Zand- en kalklandschap", 104, 'nee'],
                        ["N02.01.00", "Rivier", 201, 'nee'],
                        ["N03.01.00", "Beek en Bron", 301, 'nee'],
                        ["N04.01.00", "Kranswierwater", 401, 'nee'],
                        ["N04.02.00", "Zoete Plas", 402, 'nee'],
                        ["N04.02.01", "Zoete plas in open gebied", 40201, 'ja'],
                        ["N04.02.02", "Zoete plas in of grenzend aan bos", 40202, 'ja'],
                        ["N04.03.00", "Brak water", 403, 'nee'],
                        ["N04.04.00", "Afgesloten zeearm", 404, 'nee'],
                        ["N05.01.00", "Moeras", 501, 'nee'],
                        ["N05.01.01", "Krabbescheervelden", 50101, 'ja'],
                        ["N05.01.02", "Landriet", 50102, 'ja'],
                        ["N05.01.03", "Waterriet", 50103, 'ja'],
                        ["N05.01.04", "Hoge zeggen en biezen", 50104, 'ja'],
                        ["N05.01.05", "Veenmosrietland", 50105, 'ja'],
                        ["N05.01.06", "Moerasstruweel", 50106, 'ja'],
                        ["N05.01.07", "Moerasloofbos", 50107, 'ja'],
                        ["N05.01.08", "Moerasnaaldbos", 50108, 'ja'],
                        ["N05.01.09", "Laagveen", 50109, 'ja'],
                        ["N05.01.10", "Hoogveenbos", 50110, 'ja'],
                        ["N05.01.11", "Galigaanmoerassen", 50111, 'ja'],
                        ["N05.01.12", "Kalktufbronnen en kalkmoerassen", 50112, 'ja'],
                        ["N05.01.13", "Open zand", 50113, 'ja'],
                        ["N05.01.14", "Slikkige rivieroever", 50114, 'ja'],
                        ["N05.01.15", "Nat hakhout", 50115, 'ja'],
                        ["N05.02.00", "Gemaaid rietland", 502, 'nee'],
                        ["N06.01.00", "Veenmosrietland en moerasheide", 601, 'nee'],
                        ["N06.02.00", "Trilveen", 602, 'nee'],
                        ["N06.03.00", "Hoogveen", 603, 'nee'],
                        ["N06.04.00", "Vochtige heide", 604, 'nee'],
                        ["N06.05.00", "Zwakgebufferd ven", 605, 'nee'],
                        ["N06.06.00", "Zuur ven en hoogveenven", 606, 'nee'],
                        ["N07.01.00", "Droge heide", 701, 'nee'],
                        ["N07.02.00", "Zandverstuiving", 702, 'nee'],
                        ["N08.01.00", "Strand en embryonaal duin", 801, 'nee'],
                        ["N08.02.00", "Open duin", 802, 'nee'],
                        ["N08.02.01", "Zeereep en strand", 80201, 'ja'],
                        ["N08.02.02", "Stuivend duinzand", 80202, 'ja'],
                        ["N08.02.03", "Witte duinen", 80203, 'ja'],
                        ["N08.02.04", "Nat en vochtig duingrasland", 80204, 'ja'],
                        ["N08.02.05", "Vochtig duinvallei (kalkrijk)", 80205, 'ja'],
                        ["N08.02.06", "Vochtig duinvallei (ontkalkt)", 80206, 'ja'],
                        ["N08.02.07", "Droog duingrasland kalkrijk", 80207, 'ja'],
                        ["N08.02.08", "Droog duingrasland kalkarm", 80208, 'ja'],
                        ["N08.02.09", "Droge Duinruigte", 80209, 'ja'],
                        ["N08.02.10", "Duinriet", 80210, 'ja'],
                        ["N08.02.11", "Duinstruweel", 80211, 'ja'],
                        ["N08.02.12", "Duinnaaldbos", 80212, 'ja'],
                        ["N08.02.13", "Duinloofbos", 80213, 'ja'],
                        ["N08.02.14", "Vochtige duinheide", 80214, 'ja'],
                        ["N08.02.15", "Duingrasland", 80215, 'ja'],
                        ["N08.03.00", "Vochtige duinvallei", 803, 'nee'],
                        ["N08.04.00", "Duinheide", 804, 'nee'],
                        ["N09.01.00", "Schor of kwelder", 901, 'nee'],
                        ["N10.01.00", "Nat schraalland", 1001, 'nee'],
                        ["N10.02.00", "Vochtig hooiland", 1002, 'nee'],
                        ["N11.01.00", "Droog schraalgrasland", 1101, 'nee'],
                        ["N12.01.00", "Bloemdijk", 1201, 'nee'],
                        ["N12.02.00", "Kruiden- en faunarijk grasland", 1202, 'nee'],
                        ["N12.03.00", "Glanshaverhooiland", 1203, 'nee'],
                        ["N12.04.00", "Zilt- en overstromingsgrasland", 1204, 'nee'],
                        ["N12.05.00", "Kruiden- of faunarijke akker", 1205, 'nee'],
                        ["N12.06.00", "Ruigteveld", 1206, 'nee'],
                        ["N13.01.00", "Vochtig weidevogelgrasland", 1301, 'nee'],
                        ["N13.02.00", "Wintergastenweide", 1302, 'nee'],
                        ["N14.01.00", "Rivier- en beekbegeleidend bos", 1401, 'nee'],
                        ["N14.01.01", "Rivier- en beekbegeleidend loofbos", 140101, 'ja'],
                        ["N14.01.02", "Rivier- en beekbegeleidend naaldbos", 140102, 'ja'],
                        ["N14.01.03", "Rivier- en beekbegeleidend gemengd bos", 140103, 'ja'],
                        ["N14.01.06", "Rivier- beekbegeleidind eiken-beukenbos", 140106, 'ja'],
                        ["N14.01.07", "Rivier- en beekbegeleidend eikenhaagbeukenbos", 140107, 'ja'],
                        ["N14.01.08", "Rivier- en beekbegeleidend populierenbos", 140108, 'ja'],
                        ["N14.01.09", "Ruigte of zoom in rivier- en beekbegeleidend bos", 140109, 'ja'],
                        ["N14.02.00", "Hoog- en laagveenbos", 1402, 'nee'],
                        ["N14.02.01", "Hoog- en laagveenbos- loofbos", 140201, 'ja'],
                        ["N14.02.02", "Hoog- en laagveenbos- naaldbos", 140202, 'ja'],
                        ["N14.02.03", "Hoog- en laagveenbos- gemengd bos", 140203, 'ja'],
                        ["N14.02.04", "Laagveenbos", 140204, 'ja'],
                        ["N14.02.05", "Hoogveenbos", 140205, 'ja'],
                        ["N14.02.06", "Ruigten en zomen in hoog- en laagveenbos", 140206, 'ja'],
                        ["N14.02.08", "Hoog- en laagveenbos- populier", 140208, 'ja'],
                        ["N14.03.00", "Haagbeuken- en essenbos", 1403, 'nee'],
                        ["N15.01.00", "Duinbos", 1501, 'nee'],
                        ["N15.01.01", "Duinloofbos", 150101, 'ja'],
                        ["N15.01.02", "Duinnaaldbos", 150102, 'ja'],
                        ["N15.01.03", "Duin gemengd bos", 150103, 'ja'],
                        ["N15.01.04", "Duindoorn of kruipwilgstruwelen", 150104, 'ja'],
                        ["N15.01.06", "Duinbos- eiken", 150105, 'ja'],
                        ["N15.01.08", "Duinbos- populier", 150108, 'ja'],
                        ["N15.02.00", "Dennen-- eiken- en beukenbos", 1502, 'nee'],
                        ["N15.02.01", "Dennen-- eiken- en beukenbos- loof", 150201, 'ja'],
                        ["N15.02.02", "Dennen-- eiken- en beukenbos- naald", 150202, 'ja'],
                        ["N15.02.03", "Dennen-- eiken- en beukenbos- gemengd", 150203, 'ja'],
                        ["N15.02.04", "Dennen-- eiken- en beukenbos- beukenbos", 150204, 'ja'],
                        ["N15.02.05", "Dennen-- eiken- en beukenbos- eikenbos", 150205, 'ja'],
                        ["N15.02.06", "Dennen-- eiken- en beukenbos- eiken-beukenbos", 150206, 'ja'],
                        ["N15.02.07", "Dennen-- eiken- en beukenbos- eikenhaagbeukenbos", 150207, 'ja'],
                        ["N15.02.08", "Dennen-- eiken- en beukenbos- populier", 150208, 'ja'],
                        ["N16.01.00", "Droog bos met productie", 1601, 'nee'],
                        ["N16.01.01", "Droog bos met productie- loof", 160101, 'ja'],
                        ["N16.01.02", "Droog bos met productie- naald", 160102, 'ja'],
                        ["N16.01.03", "Droog bos met productie- gemengd", 160103, 'ja'],
                        ["N16.01.04", "Droog bos met productie- beuk", 160104, 'ja'],
                        ["N16.01.05", "Droog bos met productie- eik", 160105, 'ja'],
                        ["N16.01.06", "Droog bos met productie- eiken-beukenbos", 160106, 'ja'],
                        ["N16.01.07", "Droog bos met productie- eiken-haagbeukenbos", 160107, 'ja'],
                        ["N16.01.08", "Droog bos met productie- populier", 160108, 'ja'],
                        ["N16.02.00", "Vochtig bos met productie", 1602, 'nee'],
                        ["N16.02.01", "Vochtig bos met productie- loof", 160201, 'ja'],
                        ["N16.02.02", "Vochtig bos met productie- naald", 160202, 'ja'],
                        ["N16.02.03", "Vochtig bos met productie- gemengd", 160203, 'ja'],
                        ["N16.02.05", "Vochtig bos met productie- eikenbos", 160205, 'ja'],
                        ["N16.02.06", "Vochtig bos met productie- eiken-beukenbos", 160206, 'ja'],
                        ["N16.02.07", "Vochtig bos met productie- eiken-haagbeukenbos", 160207, 'ja'],
                        ["N16.02.08", "Vochtig bos met productie- populier", 160208, 'ja'],
                        ["N17.01.00", "Vochtig hakhout en middenbos", 1701, 'nee'],
                        ["N17.02.00", "Droog hakhout", 1702, 'nee'],
                        ["N17.03.00", "Park- of stinzenbos", 1703, 'nee'],
                        ["N17.03.01", "Park- of stinzebos- loofbos", 170301, 'ja'],
                        ["N17.03.02", "Park- of stinzebos- naaldbos", 170302, 'ja'],
                        ["N17.03.03", "Park- of stinzebos- gemengd bos", 170303, 'ja'],
                        ["N17.03.05", "Park- of stinzebos- eikenbos", 170305, 'ja'],
                        ["N17.03.06", "Park- of stinzebos- eiken-beukenbos", 170306, 'ja'],
                        ["N17.03.08", "Park- of stinzebos- populier", 170308, 'ja'],
                        ["N17.04.00", "Eendenkooi", 1704, 'nee'],
                        ["W00.01.00", "Weg en pad", 10001, 'nee'],
                        ["W00.02.00", "Overig", 10002, 'nee'],
                        ["W00.03.00", "Bebouwing", 10003, 'nee'],
                        ["W00.04.00", "Bomenrij of heg", 10004, 'nee'],
                        ["W00.05.00", "Steenglooiing of krib", 10005, 'nee'],
                        ["W00.06.00", "Agrarisch", 10006, 'nee'],
                        ["W00.07.00", "Spoor", 10007, 'nee'],
                        ["W01.01.00", "Rivierduin- open zand in riviergebied", 10101, 'nee'],
                        ["W01.02.00", "Open zand in bos", 10102, 'nee'],
                        ["W02.01.00", "Greppel", 10201, 'nee'],
                        ["W02.02.00", "Sloot tot 6 meter", 10202, 'nee'],
                        ["W02.03.00", "Sloot 6 tot 12 meter", 10203, 'nee'],
                        ["W02.04.00", "Water breder dan 12 meter", 10204, 'nee'],
                        ["W03.01.00", "Breed water", 10301, 'nee'],
                        ["W04.01.00", "Grasland in of grenzend aan bos", 10401, 'nee'],
                        ["W04.02.00", "Vochtig grasland in of grenzend aan bos", 10402, 'nee'],
                        ["W04.03.00", "Bouwland in of grenzend aan bos", 10403, 'nee'],
                        ["W05.01.00", "Jeneverbesstruweel", 10501, 'nee'],
                        ["W06.01.00", "Boomgaard", 10601, 'nee'],
                        ["W07.01.00", "Begraafplaats in bos", 10701, 'nee'],
                        ["S01.13.00", "Bomenrij of solitaire boom in kern", 15501, 'nee'],
                        ["T15.02.00", "Dennen- eiken- en beukenbos in kern", 15502, 'nee']]
        self.units = pd.DataFrame(data=nbt_contents, columns=['nbt', 'desc', 'pxl', 'neergeschaald'])
        self.pxl2nbt = dict(zip(self.units.pxl, self.units.nbt))
        self.nbt2pxl = dict(zip(self.units.nbt, self.units.pxl))

    def to_qlr(self, tab, lyr_name, out_dir, out_name, color=None):
        """
        Write qml file visualising pixels where col == val
        :tab: pd.Dataframe with Value and Description columns
        :lyr_name: string with layer name to appear in QGIS table of contents
        :out_dir: directory to write the qlr file
        :out_name: qlr file name
        :color: optional HEX color for all values in tab.Value
        :return: outdir\outname.qml
        """

        tree = ET.parse(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\MNP\draakgracht\template_bt20190612.qlr')
        root = tree.getroot()
        maplayers = root.find('maplayers')
        maplayer = maplayers.find('maplayer')
        layername = maplayer.find('layername')
        pipe = maplayer.find('pipe')
        rasterrenderer = pipe.find('rasterrenderer')
        colorpalette = rasterrenderer.find('colorPalette')

        # update colors
        for x in colorpalette.findall('paletteEntry'):
            colorpalette.remove(x)

        vals = tab.Value
        desc = tab.Description
        for v, d in zip(vals, desc):
            ET.SubElement(colorpalette, 'paletteEntry', attrib={'color': color if color else rand_web_color_hex(),
                         'value': str(v), 'label': d, 'alpha': "255"})

        # update layer name
        if hasattr(layername, 'text'):
            layername.text = lyr_name
        tree.write(os.path.join(out_dir, '{}.qlr'.format(out_name)))


def ndff_sources():

    return {'broedvogel': r'W:\PROJECTS\MNP2020\MNP_2020\c_fases\f7_draagkracht\a_source_data\vogels_ndff_shapefiles\ndff_vogels_merged_mk2.shp',
            'dagvlinder': r'W:\PROJECTS\MNP2020\MNP_2020\c_fases\f7_draagkracht\a_source_data\vlinders_ndff_shapefiles\ndff_dagvlinders_merged_mk2.shp',
            'test': r'c:\apps\temp_geodata\ndff\ndff_vogel_sample.shp'}

class Species:
    """
    Class holding all info related to a species
    """

    def __init__(self, sp_name):
        sp_info = pd.read_excel(
            r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\MNP\draakgracht\soort_info\soort_informatie.xlsx',
            sheet_name='mnp_soorten')
        self.input = sp_name
        self.name = sp_name.lower()
        nl2latin = dict(zip(sp_info.soort_ned.str.lower(), sp_info.soort_wet.str.lower()))
        latin2nl = dict(zip(sp_info.soort_wet.str.lower(), sp_info.soort_ned.str.lower()))
        soort2groep = dict(zip(sp_info.soort_ned.str.lower(), sp_info.srtgroepen))
        soort2nr = dict(zip(sp_info.soort_ned.str.lower(), sp_info.sp_nr))

        if self.name in set(sp_info.soort_wet.str.lower()):
            self.naam_ned = latin2nl[self.name]
            self.naam_wet = self.name
        elif self.name in set(sp_info.soort_ned.str.lower()):
            self.naam_ned = self.name
            self.naam_wet = nl2latin[self.name]
        else:
            raise ImportError('{} is not a recognized name'.format(self.name))

        self.soortgroep = soort2groep[self.naam_ned]
        self.soortnr = soort2nr[self.naam_ned]
        self.ndff_src = ndff_sources()[self.soortgroep]
        self.mnp_species_code = '{0}{1}'.format({'broedvogel': 'S02', 'vaatplant': 'S09',
                                                 'dagvlinder': 'S06'}[self.soortgroep], str(self.soortnr)[1:])


def colrow_2_xy(colrow, rio_object=None, affine_in=None):
    """
    https://www.perrygeo.com/python-affine-transforms.html
    :param colrow: column-index_row-index tuple
    :param rio_object: rio object
    :param affine_in: affine transformation
    :return: easting norhting in CRS
    """
    try:
        a = rio_object.affine
    except AttributeError:
        if isinstance(affine_in, affine.Affine):
            a = affine_in
        else:
            raise Exception("{0} is not a valid rasterio object and no Affine alternative supplied".format(rio_object))
    col, row = colrow
    x, y = a * (col, row)
    return x, y


def xy_2_colrow(point, rio_object):
    """
    https://www.perrygeo.com/python-affine-transforms.html
    """
    try:
        a = rio_object.affine
        x, y = point.x, point.y
        col, row = ~a * (x, y)
        return '{0}_{1}'.format(int(col//1), int(row//1))
    except AttributeError as e:
        print(e)
        raise
        # is dit nuttig?


def get_mnp_dk(species, bt):

    mnp = pd.read_csv(r'\\wur\dfs-root\PROJECTS\QMAR\MNP-SNL-ParameterSet\Parameters_v05_2019_06_12\03_MNP_versie5_par_density_factors_BT2019_v2.csv', sep=',',
                      usecols=['Species_code', 'Land_type_code', 'Land_type_quality'])
    try:
        dk = mnp.loc[(mnp.Species_code == species.mnp_species_code) & (mnp.Land_type_code == bt), 'Land_type_quality']
        if dk.empty:
            return 0
        else:
            return float(dk)
    except AttributeError:
        print('Make sure to provide a Species object, thanks.')
        raise

def valid_dir(x):
    """
    Is x an existing directory?
    :param x: path
    :return: x if True, else argparse.ArgumentTypeError
    """
    # use: parser.add_argument('datapath', type=pathlib.Path)

    if os.path.isdir(x):
        return x
    else:
        raise argparse.ArgumentTypeError('{} is not a valid directory'.format(x))




