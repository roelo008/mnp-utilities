"""
Determine draagkracht (DK) for vogel and vlinder using NDFF database
Hans Roelofsen, mei 2020

DK bestaat voor elke soort (sp)- beheertype (bt) combinatie als een getal tussen 0-1

bt brondata: dominantie beheertype kaart op 25m incl neerschaling
sp brondata: NDFF uittreksels.

Methode:
1. Reduce NDFF observations to centroids
2. Cluster 1) to 25m cells matching the BT kaart grid. Ignore observation count per cell, retain spatial distribution
   of NDFF observations only
3. Impose 5000m square grid over 2) and BT kaart
4. For each 5000m sq calculate: area_ndff_observations_within_BT / area_BT_type
5. Assign highest value of 4 for the species - BT combination

Dit is een behoorlijk lelijk scriptje geworden. Excuses.


prep of ndff data:
source data: W:\PROJECTS\MNP2020\c_fases\f7_draagkracht\a_source_data\NDFF_levering_okt2020\ndff_vogels_dagvlinders_sep_okt2020.shp
             W:\PROJECTS\MNP2020\c_fases\f7_draagkracht\a_source_data\NDFF_levering_2018\def_broedvogels.shp
             W:\PROJECTS\MNP2020\c_fases\f7_draagkracht\a_source_data\NDFF_levering_2018\def_dagvlinders.shp

# Split 2020 data into vogels and vlinders
ogr2ogr -where "srtgroepen = 'Vogels'" ndff_vogels_sep_okt2020.shp ndff_vogels_dagvlinders_sep_okt2020.shp
ogr2ogr -where "srtgroepen = 'Dagvlinders'" ndff_dagvlinders_sep_okt2020.shp ndff_vogels_dagvlinders_sep_okt2020.shp

# merge vogels 2020 and vogels 2018 into single file



"""

import os
import argparse
import numpy as np
import pandas as pd
import geopandas as gp
import rasterstats as rast
from mnp import helpers as hlp
# from sample.mnp import helpers as hlp
# sp = hlp.Species('Hooibeestje')

parser = argparse.ArgumentParser()
parser.add_argument('tax', type=str, help='taxonomic group', choices=['broedvogel', 'dagvlinder', 'test'])
parser.add_argument('species', type=str, help='species name', nargs='+')
parser.add_argument('--th', type=float, help='Minimum draagkracht', default=0.01)
parser.add_argument('--ndff_th', type=int, help='Minimum NDFF obs per BT per Uurhol', default=10)
parser.add_argument('--out_dir', type=hlp.valid_dir, help='output dir', default='./')
args = parser.parse_args()
species = args.species
tax = args.tax
out_dir = args.out_dir

if tax == 'test':
    species = ['Scholekster', 'Zanglijster', 'Braamsluiper']

# Gather data that can be used for all species
ndff_all = hlp.read_ndff_shp(src=hlp.ndff_sources()[args.tax])
bt = hlp.BT20190612()  # Beheertypenkaart (version 20190612 25m version)

for i, species in enumerate(species, start=1):

    print('Doing {0} ({1}/{2})'.format(species, i, len(args.species)))

    '''Query NDFF shapefile'''
    try:
        sp = hlp.Species(species)
        ndff = hlp.query_ndff(gdf=ndff_all, species=sp, max_area=10000, min_year=2010)  # NDFF shapefile for selected species.
    except (AssertionError, ImportError) as e:
        hlp.write2file(df=pd.Series({species: e}), out_name='error_{}'.format(species), out_dir=args.out_dir)
        continue

    csv_out_name = '03_MNP_versie7_par_density_factors_{0}'.format(sp.naam_ned)
    shp_out_basename = 'draagkracht_{}X{}.shp'
    areas = hlp.gen_squares(x_ul=0, y_ul=625000, ncol=57, nrow=64, size=5000)  # Square grid over NL
    # TODO: use benb_utils.gen_squares
    '''Reduce NDFF observations to NDFF grid cells. Then couple with Areas and BT kaart'''
    # Couple each NDFF observation to a Col-Row index from the bt_kaart using the Affine transformation.
    # Then drop duplicates to ensure only 1 NDFF observation is counted per bt_kaart pixel.
    ndff.loc[:, 'nbt_row_col'] = ndff.geometry.centroid.apply(hlp.xy_2_colrow, rio_object=bt.raster)
    pre = ndff.shape[0]
    ndff.drop_duplicates(subset='nbt_row_col', inplace=True)
    print('  aggregate to 25m pixels reduces from {0} to {1}'.format(pre, ndff.shape[0]))


    # Couple NDFF observations to one of the squares. Drop observations that are outside the squares.
    pre = ndff.shape[0]
    ndff = gp.sjoin(left_df=ndff, right_df=areas.loc[:, ['ID', 'geometry']], how='inner', op='within')
    post = ndff.shape[0]
    if pre != post:
        print('  dropping {} NDFF observations outside NL'.format(str(np.subtract(pre, post))))

    # Couple NDFF waarneming to a BT. No more than 1 BT per NDFF observation N
    # No more than 1 pixel per NDFF observation
    ndff__nbt = rast.zonal_stats(vectors=ndff.geometry.centroid, raster=bt.raster.read(1), categorical=True,
                                 all_touched=False, category_map=bt.pxl2nbt, affine=bt.raster.affine, nodata=0)
    try:
        assert all([len(x) == 1 for x in ndff__nbt]), 'NDFF observations are matched to > 1 BT'
        assert all([item for sublist in [[v == 1 for v in x.values()] for x in ndff__nbt] for item in sublist]), 'Some NDFF obs are not matched to a BT'
    except AssertionError as e:
        hlp.write2file(df=pd.Series({sp.naam_ned: e}), out_name='error_{}'.format(csv_out_name), out_dir=args.out_dir)
        continue
    ndff.loc[:, 'nbt'] = [item for sublist in [[k for k in x.keys()] for x in ndff__nbt] for item in sublist]

    ndff.dropna(subset=['nbt'], inplace=True)

    '''Determine acreage per BT for each hok.'''
    areas__nbt = rast.zonal_stats(vectors=areas, raster=bt.raster.read(1), categorical=True, all_touched=False,
                                  category_map=bt.pxl2nbt, affine=bt.raster.affine, nodata=0)

    # pixel counts for each area should sum to 5000^2 / 25^2 = 40.000 or 0 when area is outside the BT-kaart
    try:
        assert all([np.sum(v for _, v in area_dict.items()) in [40000, 0] for area_dict in areas__nbt])
    except AssertionError as e:
        print(e)
        continue

    # Make a list with len == areas.shape[0] where each item is a dictionary w. bt.units.nbt as keys and
    # pixel count of nbt X area as values. Then join to the areas gdf
    bt_stats = []
    for area_stats in areas__nbt:
        empty_stats = dict.fromkeys(bt.units.nbt, np.nan)
        # See: https://stackoverflow.com/questions/38987/how-do-i-merge-two-dictionaries-in-a-single-expression-in-python
        bt_stats.append({**empty_stats, **area_stats})
    areas = areas.join(pd.DataFrame.from_dict(bt_stats, orient='columns'))
    # areas df now has all nbt units as as columns with pixel count per area as values

    '''Pivot NDFF waarnemingen to Area ids, nbt as columns and count of ndff pixel IDs as values. Merge with areas df.'''
    try:
        assert set(ndff.nbt).issubset(areas.columns), "NDFF columns are not a subset of beheertypen, weird..."
    except AssertionError as e:
        print(e)
        continue

    ndff_per_area = pd.pivot_table(data=ndff.assign(id=1), index='ID', columns='nbt', values='id', aggfunc='count')
    ndff_per_area.columns = ['{0}_ndffpxls'.format(x) for x in ndff_per_area.columns.tolist()]

    '''Merge NDFF observations df with areas df for final df where rows=areas, with cols for BT count and BTxNDFF count'''
    areas = pd.merge(left=areas, right=ndff_per_area, left_on='ID', right_index=True, how='left')

    '''Calculate Draagkracht as the largest ratio bt-pixels/ndff-pixels per area. Ignore NoData'''
    dks = {}
    for beheertype in set(bt.units.nbt).difference(set(['nodata'])):
        try:
            ndff_pxls_col = '{}_ndffpxls'.format(beheertype)
            bt_pxls_col = beheertype

            # Subset from areas to Columns relating to beheertype and rows where both NDFF and BT are not NA
            area_sel = areas.loc[:, [ndff_pxls_col, bt_pxls_col, 'geometry', 'ID']] \
                            .dropna(axis=0, subset=[ndff_pxls_col, bt_pxls_col], how='any')
            area_sel['uh_dk'] = area_sel[ndff_pxls_col].divide(area_sel[bt_pxls_col]).round(3)
            area_sel['dk_gte_{}'.format(args.ndff_th)] = np.where(area_sel[ndff_pxls_col] >= args.ndff_th, 1, 0)

            # Final draagkracht voor Soort X Beheertype combinatie
            draagkracht = area_sel.loc[area_sel.dk_gte_10 == 1, 'uh_dk'].max()

            area_sel['fin_dk'] = np.where(area_sel['uh_dk'] == draagkracht, draagkracht, 0)
            area_sel.rename(columns={ndff_pxls_col: 'ndff_count', bt_pxls_col: 'bt_count'})\
                    .to_file(os.path.join(args.out_dir, shp_out_basename.format(sp.naam_ned, beheertype.replace('.', ''))))

            if draagkracht >= args.th:
                dks[beheertype] = {'Species_code': sp.mnp_species_code, 'Land_type_code': beheertype,
                                   'Land_type_quality': draagkracht}
            else:
                continue
        except (KeyError, ValueError):  # ValueError when area_sel.empty
            continue

    out = pd.DataFrame.from_dict(dks, orient='index')
    if not out.empty:
        print('  {0} matched to {1} beheertypen'.format(sp.naam_ned, out.shape[0]))
        hlp.write2file(df=out, out_name=csv_out_name, out_dir=args.out_dir)
    else:
        print('  zero dk for {}'.format(sp.naam_ned))


