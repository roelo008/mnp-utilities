  Metadata voor
LU_scenarioA_WEnR_20200528.flt
LU_scenarioA_WEnR_20200528.hdr

  Gemaakt
Hans Roelofsen, Wageningen Environmental Research, team Biodiversiteit & Beleid
dd 28 mei 2020. Gemaakt als onderdeel van WEnR project 'Evaluatie van de Leefomgevingsverkenning'

  Omschrijving
Dit bestand bevat landgebruikscategorieen voor heel Nederland zoals omschreven in scenario A. 

  Totstandkoming
Deze kaart is gebasseerd op:
1) TOP10NL, april 2020: https://www.pdok.nl/introductie/-/article/basisregistratie-topografie-brt-topnl
2) Natuurbeheerplan 2020: https://www.bij12.nl/onderwerpen/natuur-en-landschap/kaarten-provincies-bekijken/
Zie voor analysestappen: https://git.wur.nl/roelo008/mnp-utilities/-/tree/master/sample/voorbeeld.py

  Legenda
Categorie <-> Pixelwaarde
Non-perennial plants			1
Perennial plants			2
Greenhouses				3
Meadows (grazing)			4
Bushes and hedges bordering fields	5
Dunes with permanent vegetation		11
Active coastal dunes			12
Beach					13
Deciduous forest			21
Coniferous forest			22
Mixed forest				23
Heath land				24
Inland dunes				25
Fresh water wetland			26
(semi) Natural grassland		27
Public green space			28
Other unpaved terrain			29
River flood basin			31
Salt marsh				32
Residential area			41
Roads - other				45
Sea					51
Lakes and ponds				52
Rivers and streams			53

  Technische Specificaties
Size is 11200, 13000
Origin = (0.000000000000000,625000.000000000000000)
Pixel Size = (25.000000000000000,-25.000000000000000)
Corner Coordinates:
Upper Left  (       0.000,  625000.000)
Lower Left  (       0.000,  300000.000)
Upper Right (  280000.000,  625000.000)
Lower Right (  280000.000,  300000.000)
Center      (  140000.000,  462500.000)
Band 1 Block=11200x1 Type=Float32, ColorInterp=Undefined
  NoData Value=-9999

  Meer info
Contact Hans Roelofsen: https://research.wur.nl/en/persons/hans-roelofsen


