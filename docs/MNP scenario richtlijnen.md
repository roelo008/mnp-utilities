# Richtlijnen MNP scenario's
*Aanleveren van scenariokaarten voor MNP kan lastig zijn. Volg de aanbevelingen in dit artikel om jouw scenario probleemloos door te rekenen met MNP.*
### Over MNP
In opdracht van het [PBL](https://www.pbl.nl/) heeft het team Biodiversiteit & Beleid (B&B) van Wageningen Environmental Research [WEnR](https://www.wur.nl/nl/Onderzoek-Resultaten/Onderzoeksinstituten/Environmental-Research.htm) het model MNP (Model for Nature Policy) ontwikkeld, met als doel evaluatie van het Nederlandse natuurbeleid. MNP modelleert het voorkomen van planten-, vogel- en vlindersoorten in het Nederlandse landschap en bepaalt of een soort daarin duurzaam kan voortbestaan. Het percentage duurzaam voorkomende soorten is een belangrijke uitkomst van MNP. 
De invoerdata (input) van MNP bestaat uit kaarten en tabellen. De kaarten schetsen een beeld van de natuurlijke leefomgeving waarin de soorten kunnen voorkomen. De tabellen beschrijven de vereisten die de soorten stellen aan hun leefomgeving, zoals minimum areaal, minimale en maximale pH of minimale en maximale GVG. 
### Scenarios
Een scenario schetst een toekomstige staat van het land als gevolg van bepaalde beleidskeuzes. Bijvoorbeeld: er is veel nieuwe natuur aangelegd, of de N depositie is toegenomen. Scenario's worden doorgaans ontwikkeld binnen een project waarin WEnR samenwerkt met een derde partij. Bijvoorbeeld, in de Natuurverkenningen werkt WEnR samen met het PBL.
MNP kan scenario's evalueren door te berekenen hoezeer het duurzaam voorkomen van soorten verschilt ten opzichte van andere scenario's of de uitgangssituatie. Hiervoor is het wel belangrijk dat scenarios hun weerslag hebben op het landgebruik en/of de parameters waarmee MNP het duurzaam voorkomen van soorten bepaalt; het scenario moet ingetekend worden op een of meer van de MNP inputkaarten. 

#### Kies de juiste kaarten
De inputkaarten van MNP zijn als volgt:
1. **Beheertypenkaart** Op deze kaart staat de verspreiding van verschillende beheertypen waarin de soorten kunnen voorkomen. Deze kaart wordt ook wel de *Neergeschaalde beheertypen-* of de *BT kaart* genoemd. In de technische documentatie wordt deze kaart omschreven als de *Landschapstypenkaart*. 
2. **GVG-kaart** Gemiddelde voorjaarsgrondwaterstand in cm beneden maaiveld
3. **N-depositie kaart** in mmol/ha
4. **pH-kaart** 
5. **Temperatuurkaart** in graden Celcius (optioneel). 

De Beheertypenkaart beschrijft de **biotische** omstandigheden, de overige kaarten beschrijven de **abiotische** omstandigheden. 

Een scenario **moet** zijn weerslag hebben op ten minste een van de inputkaarten. Kaarten die geen onderdeel zijn van het scenario blijven natuurlijk wel verreist om MNP te draaien. Hiervoor zijn standaardkaarten beschikbaar. Overleg met het WEnR MNP team hierover. 
#### Zorg voor ruimtelijke consistentie tussen de kaarten
De kaarten **moeten** in ruimtelijke zin exact overeen komen, zodat de pixel in rij X kolom Y altijd correspondeert met dezelfde locatie. 
De kaarten hoeven niet landsdekkend te zijn; er mag NoData in staan. Echter, enkel pixels waarvan alle kaarten een geldige waarde geven worden geanalyseerd door MNP (zie [1] voor geldige waardes van de abiotische kaarten). In de praktijk betekent dit dat overal waar een beheertype ligt, de GVG, pH, NDep en temperatuur gegeven **moeten** zijn. Let dus op als er nieuwe natuur wordt ingetekend; daar **moet** ook abiotiek gegeven worden. Als de scenario-ontwikkelaars zelf slechts de beheertypekaart aanpassen, vraag dan het WEnR team naar de mogelijkheden die er zijn om de abiotiek-kaarten aan te passen. De figuren hieronder illustreren de verreisten van MNP omtrent de invoerkaarten.

![rasters komen exact overeen](docs/images/mnp_kaart_match.jpg)

![MNP rekent alleen voor cellen met volledige informatie](docs/images/mnp_valid_cells.jpg)

#### Volg de tech-specs
Alle kaarten zijn [rasterdata](https://desktop.arcgis.com/en/arcmap/10.3/manage-data/raster-and-images/what-is-raster-data.htm). De kaarten **moeten** als volgt worden aangeleverd:
* Bestandsformaat is ESRI [*.flt](https://desktop.arcgis.com/en/arcmap/10.3/tools/conversion-toolbox/raster-to-float.htm) of ESRI [*.BIL](https://desktop.arcgis.com/en/arcmap/10.3/manage-data/raster-and-images/bil-format-example.htm). Let erop dat naast de *.bil en *.flt bestand ook een *.hdr bestand vereist is. 
* Pixelgrootte
  * 25m *of* 2.5m voor de beheertypenkaart
  * 25m voor alle andere kaarten
* Aantal rijen en kolommen
  * 130.000 x 112.000 voor 2.5m pixelgrootte
  * 13.000 x 11.200 voor 25m pixelgrootte
* Extent in m volgens [RD-New](https://zakelijk.kadaster.nl/rijksdriehoeksmeting) (zie figuur hieronder): 
  * links: 0
  * onder: 300.000
  * rechts: 280.000
  * boven: 625.000
 
![Positionering van rasters binnen RD-New](docs/images/image_extent.jpg)

Geef bestanden bij voorkeur een naam die de kaart duidelijk omschrijft: scenarionaam, kaarttype en maker. Voeg datum-tijdstip toe om opeenvolgende versies van een bestand te onderscheiden. Een voorbeeld bestandsnaam kan zijn: **`<scenarionaam>_<kaarttype>_<producent>_<datum><tijd>.flt`**: *scenarioA_BT_PBL_202004151201.flt*. Voor aanvullende details over de inputdata, raadpleeg Jochem (2016) [1].
#### Zonder Metadata geen data
Elke scenariokaart **moet** vergezeld gaan van metadata waarin wordt beschreven:
* bestandsnamen van de kaart 
* wie heeft de kaart gemaakt
* wanneer is de kaart gemaakt
* waarom is de kaart gemaakt (welk project)
* wat staat er in de kaart (BT, GVG etc?) en volgens welk scenario
* technische details (gebruik hiervoor bijvoorbeeld [gdalinfo](https://gdal.org/programs/gdalinfo.html#gdalinfo))
* contactgegevens 

Zie [hier](https://git.wur.nl/roelo008/mnp-utilities/-/blob/master/docs/demo_README.txt) voor een voorbeeld van een ReadMe bestand. 

#### Respecteer abiotische wetmatigheden
Bij voorkeur geven de kaarten tezamen een realistisch en plausibel beeld van de standplaats. Dat betekent bijvoorbeeld dat een moerasachtige vegetatie in de beheertypenkaart weerspiegeld wordt met een lage waarde in de GVG kaart. MNP controleert niet of dit soort abiotische wetmatigheden worden gerespecteerd, maar zal als uitkomst hebben dat dit soort plekken geen geschikte leefgebieden zijn voor de soorten waar het waarschijnlijk om te doen was.
#### Gebruik de juiste bouwstenen voor de beheertypenkaart
Verschillende categorisaties zijn mogelijk om natuur in Nederland te beschrijven; van simpel (bos, heide, moeras, gras) tot complex (e.g. de plantengemeenschappen van de 'Vegetatie van Nederland'). MNP hanteert momenteel de '[Index Natuur en Landschap](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/)', met daarin:
* [Natuurtypen](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/#onderdeel-natuurtypen) (N-typen)
* [Landschapselementtypen](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/#onderdeel-landschapselementtypen) (L-typen) 
* [Agrarische natuurtypen](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/#onderdeel-agrarische-natuurtypen) (A-typen) 

N-, -L en A- typen worden gezamenlijk aangeduid als: '*beheertypen*'. Beheertypen worden geïdentificeerd met een alfanumerieke code en een naam, bijvoorbeeld [`N03.01 Beek en Bron`](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/natuurtypen/n03-beken-en-bronnen/n03-01-beek-en-bron/). 

Sommige beheertypen zijn door WEnR te grof bevonden om op een betrouwbare manier soorten aan te koppelen. Deze zijn daarom verfijnd (ook wel: *neergeschaald*). Neerschaling kan betekenen dat een van de 'Grootschalige, dynamische natuur' typen ([N01](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/natuurtypen/n01-grootschalige-dynamische-natuur/)) wordt opgedeeld naar de andere beheertypen (zie figuur hieronder), of dat een beheertype wordt verfijnd tot meerdere, nieuwe, beheertypen (ook wel: *neergeschaalde beheertypen*). Neergeschaalde beheertypen zijn te herkennen aan twee extra cijfers groter dan 0 in de code. Bijvoorbeeld `N05.01.10 Hoogveenbos` en `N05.01.05 Veenmosrietland` zijn neerschalingen van [`N05.01 Moeras`](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/natuurtypen/n05-moerassen/n05-01-moeras/). Voor consistentie in de codering worden originele beheertypencodes aangevuld met twee nullen. Bijvoorbeeld  `N03.01 Beek en Bron` wordt genoteerd als `N03.01.00 Beek en Bron`. Tenslotte heeft WEnR de beheertypen aangevuld met W-typen naar eigen ontwerp. Tabel 2 onderaan dit artikel geeft een volledig overzicht. 

![Voorbeeld neerschaling](docs/images/Voorbeeld_neeraschaling_resized2.jpg)

Grootschalig beheertype N01.04 (oranje, boven) is neergeschaald tot de omliggende beheertypen.

Bij het samenstellen van de Beheertypenkaart voor een scenario **moet** de categorisatie van de neergeschaalde beheertypen worden gebruikt. Beheertypen **mogen** achterwege blijven, maar **mogen niet** worden samengevoegd of opgedeeld. Beheertypen mogen **wel** hernoemd worden middels een alias. 

Het is in beginsel **niet** mogelijk om nieuwe beheertypen te definieren, tenzij dit in nauw overleg met WEnR gebeurt. De draagkracht van het nieuwe type ten opzichte van alle soorten in MNP zal moeten worden bepaald door de verschillende soort-experts binnen WEnR. Hiervoor **moet** ruimte zijn in de projectbegroting. Het figuur hieronder illustreert hoe scenario's vrij zijn om de beheertypen ruimtelijk te herordenen, maar in beginsel niet om nieuwe beheertypen te definieren. 

![Voorbeeld neerschaling](docs/images/mnp_bouwstenen.jpg)

De beheertypenkaart bevat een numerieke waarde per pixel. Daarom **moet** er een legenda van de beheertypenkaart worden geleverd waarin de pixelwaardes worden gerelateerd aan de beheertypen. Tabel 1 laat zien hoe deze tabel eruit kan zien.


#### Tabel 1
**pixelwaarde in de kaart**|**Beheertype**
---|---
0|Geen
1|N03.01.00
2|N04.02.00
3|N05.01.09
...|...
_n_|N17.02.00

### Tenslotte
Blijf altijd in nauw [contact](https://www.wur.nl/en/Research-Results/Research-Institutes/Environmental-Research/Contact.htm) met het WEnR MNP team voor vragen, advies en overleg. 


#### Tabel 2
De beschikbare beheertypen op dit moment (april 2020) zijn als volgt:

**Beheertype code**|**Beheertype Naam**|**opmerking**
---|---|---
A01.01.00|Weidevogelgebied
A01.02.00|Akkerfaunagebied
A01.03.00|Ganzenfourageergebied
A01.04.00|Insectenrijk grasland
A02.01.00|Botanisch waardevol grasland
A02.02.00|Botanisch waardevol akkerland
A11.02.00|Weidevogelland met riet of opgaande begroeiing
A12.01.00|Open akkerland voor broedende akkervogels
A12.02.00|Open akkerland voor overwinterende akkervogels
L01.01.00|Poel en kleine historische wateren
L01.02.00|Houtwal en houtsingel
L01.03.00|Elzensingel
L01.04.00|Bossingel en bosje
L01.05.00|Knip- of scheerheg
L01.06.00|Struweelhaag
L01.07.00|Laan
L01.08.00|Knotboom
L01.09.00|Hoogstamboomgaard
L01.10.00|Struweelrand
L01.11.00|Hakhoutbosje
L01.12.00|Griendje
L01.13.00|Bomenrij of solitaire boom
L01.14.00|Rietzoom en klein rietperceel
L01.15.00|Natuurvriendelijk oever
L02.01.00|Fortterrein
L02.02.00|Historisch bouwwerk en erf
L02.03.00|Historische tuin
L03.01.00|Aardwerk en groeve
L04.01.00|Wandelpad over boerenland
N00.01.00|Nog om te vormen landbouwgrond naar natuur (inrichting)
N00.02.00|Nog om te vormen natuur naar natuur (functieverandering)
N01.01.00|Zee en wad
N01.02.00|Duin- en kwelderlandschap
N01.03.00|Rivier- en moeraslandschap
N01.04.00|Zand- en kalklandschap
N02.01.00|Rivier
N03.01.00|Beek en Bron
N04.01.00|Kranswierwater
N04.02.00|Zoete Plas
N04.03.00|Brak water
N04.04.00|Afgesloten zeearm
N05.01.00|Moeras
N05.01.01|Krabbescheervelden|neergeschaald
N05.01.02|Landriet|neergeschaald
N05.01.03|Waterriet|neergeschaald
N05.01.04|Hoge zeggen en biezen|neergeschaald
N05.01.05|Veenmosrietland|neergeschaald
N05.01.06|Moerasstruweel|neergeschaald
N05.01.07|Moerasloofbos|neergeschaald
N05.01.08|Moerasnaaldbos|neergeschaald
N05.01.09|Laagveen|neergeschaald
N05.01.10|Hoogveenbos|neergeschaald
N05.01.11|Galigaanmoerassen|neergeschaald
N05.01.12|Kalktufbronnen en kalkmoerassen|neergeschaald
N05.01.13|Open zand|neergeschaald
N05.01.14|Slikkige rivieroever|neergeschaald
N05.01.15|Nat hakhout|neergeschaald
N05.02.00|Gemaaid rietland
N06.01.00|Veenmosrietland en moerasheide
N06.02.00|Trilveen
N06.03.00|Hoogveen
N06.04.00|Vochtige heide
N06.05.00|Zwakgebufferd ven
N06.06.00|Zuur ven en hoogveenven
N07.01.00|Droge heide
N07.02.00|Zandverstuiving
N08.01.00|Strand en embryonaal duin
N08.02.00|Open duin
N08.02.01|Zeereep en strand|neergeschaald
N08.02.02|Stuivend duinzand|neergeschaald
N08.02.03|Witte duinen|neergeschaald
N08.02.04|Nat en vochtig duingrasland|neergeschaald
N08.02.05|Vochtig duinvallei (kalkrijk)|neergeschaald
N08.02.06|Vochtig duinvallei (ontkalkt)|neergeschaald
N08.02.07|Droog duingrasland kalkrijk|neergeschaald
N08.02.08|Droog duingrasland kalkarm|neergeschaald
N08.02.09|Droge Duinruigte|neergeschaald
N08.02.10|Duinriet|neergeschaald
N08.02.11|Duinstruweel|neergeschaald
N08.02.12|Duinnaaldbos|neergeschaald
N08.02.13|Duinloofbos|neergeschaald
N08.02.14|Vochtige duinheide|neergeschaald
N08.02.15|Duingrasland|neergeschaald
N08.03.00|Vochtige duinvallei
N08.04.00|Duinheide
N09.01.00|Schor of kwelder
N10.01.00|Nat schraalland
N10.02.00|Vochtig hooiland
N11.01.00|Droog schraalgrasland
N12.01.00|Bloemdijk
N12.02.00|Kruiden- en faunarijk grasland
N12.03.00|Glanshaverhooiland
N12.04.00|Zilt- en overstromingsgrasland
N12.05.00|Kruiden- of faunarijke akker
N12.06.00|Ruigteveld
N13.01.00|Vochtig weidevogelgrasland
N13.02.00|Wintergastenweide
N14.01.00|Rivier- en beekbegeleidend bos
N14.02.00|Hoog- en laagveenbos
N14.03.00|Haagbeuken- en essenbos
N15.01.00|Duinbos
N15.02.00|"Dennen-, eiken- en beukenbos"
N16.01.00|Droog bos met productie
N16.02.00|Vochtig bos met productie
N17.01.00|Vochtig hakhout en middenbos
N17.02.00|Droog hakhout
N17.03.00|Park- of stinzenbos
N17.04.00|Eendenkooi
W00.01.00|Weg en pad|door WEnR vastgesteld type
W00.02.00|Overig|_idem_
W00.03.00|Bebouwing|_idem_
W00.04.00|Bomenrij of heg|_idem_
W00.05.00|Steenglooiing of krib|_idem_
W00.06.00|Agrarisch|_idem_
W00.07.00|Spoor|_idem_
W01.01.00|Rivierduin, open zand in riviergebied|_idem_
W02.01.00|Sloten|_idem_
W03.01.00|Breed water|_idem_



[1]: Jochem, R (2016) LARCH-MNP 4.0.2 Technical documentation LARCH-MNP (grid based model) version 4.0.2. Statutory Research Tasks Unit for Nature & Environment, Wageningen.
