Hieronder de relatie pixelwaarde, nieuwe beheertypecode, bestaande beheertypecode en beheertypeomschrijving. Deze tabel heeft betrekking op: `~\qmar\MNP-Balans2019\Neerschalingen_export.gdb\BNL_NS_Grootschalig_OpenDuin_Moeras_20190730time111603_Dominant25m_All` en is mede gebaseerd op `w:\PROJECTS\QMAR\MNP-SNL-SpatialData\bnl_ns_grootschalig_openduin_moeras_20190730time111603_lookupbtnr_waternh\Werkwijze Neerschaling voor Balans 2020 versie2.docx`


**pixelwaarde**|**BT code 20190730**|**BT code nBT2016**|**omschrijving**
---|---|---|---
101|N01.01|N01.01|Zee en wad
10228|N01.02.28|N01.01|grootschalig duin, zee en wad
10269|N01.02.69|N01.01|grootschalig duin, slik- en zandplaten 
80269|N08.02.69|N01.01|open duin, slik- en zandplaten 
102|N01.02|N01.02|Duin- en kwelderlandschap
103|N01.03|N01.03|Rivier- en moeraslandschap
104|N01.04|N01.04|Zand- en kalklandschap
201|N02.01|N02.01|Rivier
301|N03.01|N03.01|Beek en Bron
401|N04.01|N04.01|Kranswierwater
402|N04.02|N04.02|Zoete Plas
10320|N01.03.20|N04.02|grootschalig moeras, plas en meer
10420|N01.04.20|N04.02|grootschalig zand, plas en meer
50120|N05.01.20|N04.02|moeras, plas en meer
403|N04.03|N04.03|Brak water
404|N04.04|N04.04|Afgesloten zeearm
501|N05.01|N05.01|Moeras
50197|N05.01.97|N05.01.00|moeras, overige natuur
10330|N01.03.30|N05.01.02|grootschalig moeras, riet
10430|N01.04.30|N05.01.02|grootschalig zand, riet
50130|N05.01.30|N05.01.02|moeras, riet
10332|N01.03.32|N05.01.03|grootschalig moeras, waterriet
50132|N05.01.32|N05.01.03|moeras, waterriet
10311|N01.03.11|N05.01.06|grootschalig moeras, ruigten en zoomen
10331|N01.03.31|N05.01.06|grootschalig moeras, riet ruigten en zoomen
50111|N05.01.11|N05.01.06|moeras, ruigten en zoomen
50131|N05.01.31|N05.01.06|moeras, riet ruigten en zoomen
50134|N05.01.34|N05.01.11|moeras, galigaan
10328|N01.03.28|N05.01.13|grootschalig moeras, droogvallend, zand
10315|N01.03.15|N05.01.14|grootschalig moeras, slikkige oever met grasvegetatie
10321|N01.03.21|N05.01.14|grootschalig moeras, slikkige oever
502|N05.02|N05.02|Gemaaid rietland
601|N06.01|N06.01|Veenmosrietland en moerasheide
50116|N05.01.16|N06.01|moeras, trilvenen en veenmosrietland
50133|N05.01.33|N06.01|moeras, veenmosrietland
50150|N05.01.50|N06.01|moeras, heide
602|N06.02|N06.02|Trilveen
603|N06.03|N06.03|Hoogveen
10451|N01.04.51|N06.03|grootschalig zand, actieve en herstellende hoogvenen
604|N06.04|N06.04|Vochtige heide
605|N06.05|N06.05|Zwakgebufferd ven
10426|N01.04.26|N06.05|grootschalig zand, zwakgebufferd ven
606|N06.06|N06.06|Zuur ven en hoogveenven
10425|N01.04.25|N06.06|grootschalig zand, zuur ven
701|N07.01|N07.01|Droge heide
10450|N01.04.50|N07.01|grootschalig zand, (kraai)heide
702|N07.02|N07.02|Zandverstuiving
10440|N01.04.40|N07.02|grootschalig zand, zand 
801|N08.01|N08.01|Strand en embryonaal duin
10242|N01.02.42|N08.01|grootschalig duin, embryonale duinen 
10260|N01.02.60|N08.02.00|grootschalig duin, open duin
80260|N08.02.60|N08.02.00|open duin, open duin 
80297|N08.02.97|N08.02.00|open duin, overige natuur
10240|N01.02.40|N08.02.02|grootschalig duin, zand 
10261|N01.02.61|N08.02.02|grootschalig duin, embryonale duinen in open duin
80240|N08.02.40|N08.02.02|open duin, zand 
80261|N08.02.61|N08.02.02|open duin, embryonale duinen in open duin
10262|N01.02.62|N08.02.03|grootschalig duin, witte duinen 
80262|N08.02.62|N08.02.03|open duin, witte duinen 
10263|N01.02.63|N08.02.07|grootschalig duin, grijze duinen (kalkrijk) 
80263|N08.02.63|N08.02.07|open duin, grijze duinen (kalkrijk) 
10264|N01.02.64|N08.02.08|grootschalig duin, grijze duinen (kalkarm) 
80217|N08.02.17|N08.02.08|open duin, grasland in grijze duinen (kalkrijk)
80218|N08.02.18|N08.02.08|open duin, grasland in grijze duinen
80264|N08.02.64|N08.02.08|open duin, grijze duinen (kalkarm) 
10266|N01.02.66|N08.02.11|grootschalig duin, duindoorn- of kruipwilgstruwelen 
80266|N08.02.66|N08.02.11|open duin, duindoorn- of kruipwilgstruwelen 
10210|N01.02.10|N08.02.15|grootschalig duin, grasland
80210|N08.02.10|N08.02.15|open duin, grasland 
803|N08.03|N08.03|Vochtige duinvallei
10214|N01.02.14|N08.03|grootschalig duin, nat grasland 
10220|N01.02.20|N08.03|grootschalig duin, plas en meer
10230|N01.02.30|N08.03|grootschalig duin, duinvallei met riet
10267|N01.02.67|N08.03|grootschalig duin, vochtige duinvalleivegetatie
50160|N05.01.60|N08.03|moeras, in duingebied
80214|N08.02.14|N08.03|open duin, nat grasland 
80220|N08.02.20|N08.03|open duin, plas en meer
80267|N08.02.67|N08.03|open duin, vochtige duinvalleivegetatie
804|N08.04|N08.04|Duinheide
10250|N01.02.50|N08.04|grootschalig duin, (kraai)heide
10265|N01.02.65|N08.04|grootschalig duin, heidevegetatie in open duin
80250|N08.02.50|N08.04|open duin, (kraai) heide 
80265|N08.02.65|N08.04|open duin, heidevegetatie in open duin
901|N09.01|N09.01|Schor of kwelder
10212|N01.02.12|N09.01|grootschalig duin, kwelder
10229|N01.02.29|N09.01|grootschalig duin, zilte pioniervegetatie zee en wad 
10241|N01.02.41|N09.01|grootschalig duin, zilte pioniervegetatie op zand 
10268|N01.02.68|N09.01|grootschalig duin, zilte pioniervegetatie in open duin
1001|N10.01|N10.01|Nat schraalland
1002|N10.02|N10.02|Vochtig hooiland
1101|N11.01|N11.01|Droog schraalgrasland
10313|N01.03.13|N11.01|grootschalig moeras, stroomdalgrasland
1201|N12.01|N12.01|Bloemdijk
1202|N12.02|N12.02|Kruiden- en faunarijk grasland
10310|N01.03.10|N12.02|grootschalig moeras, grasland
10410|N01.04.10|N12.02|grootschalig zand, grasland
50110|N05.01.10|N12.02|moeras, grasland
1203|N12.03|N12.03|Glanshaverhooiland
1204|N12.04|N12.04|Zilt- en overstromingsgrasland
10312|N01.03.12|N12.04|grootschalig moeras, zilte graslanden
1205|N12.05|N12.05|Kruiden- of faunarijke akker
1206|N12.06|N12.06|Ruigteveld
1301|N13.01|N13.01|Vochtig weidevogelgrasland
1302|N13.02|N13.02|Wintergastenweide
1401|N14.01|N14.01|Rivier- en beekbegeleidend bos
10371|N01.03.71|N14.01|grootschalig moeras, rivier en beekbegeleidend bos
1402|N14.02|N14.02|Hoog- en laagveenbos
10370|N01.03.70|N14.02|grootschalig moeras, overig bos
50170|N05.01.70|N14.02|moeras,overig bos
1403|N14.03|N14.03|Haagbeuken- en essenbos
1501|N15.01|N15.01|Duinbos
10270|N01.02.70|N15.01|grootschalig duin, overig bos
80270|N08.02.70|N15.01.01|open duin, overig bos 
80280|N08.02.80|N15.01.02|open duin, naaldbos 
1502|N15.02|N15.02|Dennen-, eiken- en beukenbos
10470|N01.04.70|N15.02|grootschalig zand, overig bos
1601|N16.01|N16.01|Droog bos met productie (vervallen)
1602|N16.02|N16.02|Vochtig bos met productie (vervallen)
1603|N16.03|N16.03|Droog bos met producties (nieuw per 01-01-2018)
1604|N16.04|N16.04|Vochtig bos met producties (nieuw per 01-01-2018)
1701|N17.01|N17.01|Vochtig hakhout en middenbos
1702|N17.02|N17.02|Droog hakhout
1703|N17.03|N17.03|Park- of stinzenbos
1704|N17.04|N17.04|Eendenkooi
1705|N17.05|N17.05|Wilgengriend (nieuw per 1-1-2017)
1706|N17.06|N17.06|Vochtig en hellinghakhout (nieuw per 1-1-2017)
10298|N01.02.98|W00.01|grootschalig duin, infrastructuur
10398|N01.03.98|W00.01|grootschalig moeras, infrastructuur
10498|N01.04.98|W00.01|grootschalig zand, infrastructuur
50198|N05.01.98|W00.01|moeras, infrastructuur
80298|N08.02.98|W00.01|open duin, infrastructuur 
10299|N01.02.99|W00.02|grootschalig duin, overig grondgebruik
10399|N01.03.99|W00.02|grootschalig moeras, overig grondgebruik
10499|N01.04.99|W00.02|grootschalig zand, overig grondgebruik
50199|N05.01.99|W00.02|moeras, overig grondgebruik
80299|N08.02.99|W00.02|open duin, overig grondgebruik 
