Hieronder een tabel met alle courante soorten voor MNP. De tabel bevat **468** soorten: **35** vlinders, **107** vogels en **326** planten. De Soortcode is opgemaakt voor gebruik in MNP (beginnend met `S0`). De laatste getallen van de code zijn vergelijkbaar met de code uit de [CBS BioBase naamlijst](https://www.cbs.nl/nl-nl/onze-diensten/methoden/classificaties/overig/biobase-2003/naamlijst-biobase). Deze tabel is gebasseerd op `\qmar\MNP-SNL-ParameterSet\Parameters_v06_2019_12_09\09_MNP_versie4_Group_Species_valid model_468.csv`

|volgnummer|Species_code|Scientific_name|Local_name|Taxon_group|
|---|---------|------------------------|--------------------|---------------|
|1|S06000012|Pyrgus malvae|Aardbeivlinder|Dagvlinder|
|2|S06000092|Lasiommata megera|Argusvlinder|Dagvlinder|
|3|S06000002|Carterocephalus palaemon|Bont dikkopje|Dagvlinder|
|4|S06000052|Aricia agestis|Bruin blauwtje|Dagvlinder|
|5|S06000008|Erynnis tages|Bruin dikkopje|Dagvlinder|
|6|S06000101|Maniola jurtina|Bruin zandoogje|Dagvlinder|
|7|S06000044|Lycaena tityrus|Bruine vuurvlinder|Dagvlinder|
|8|S06000062|Maculinea nausithous|Donker pimpernelblauwtje|Dagvlinder|
|9|S06000082|Argynnis niobe|Duinparelmoervlinder|Dagvlinder|
|10|S06000003|Thymelicus sylvestris|Geelsprietdikkopje|Dagvlinder|
|11|S06000057|Maculinea alcon|Gentiaanblauwtje|Dagvlinder|
|12|S06000035|Callophrys rubi|Groentje|Dagvlinder|
|13|S06000007|Ochlodes faunus|Groot dikkopje|Dagvlinder|
|14|S06000065|Limenitis populi|Grote ijsvogelvlinder|Dagvlinder|
|15|S06000084|Argynnis aglaja|Grote parelmoervlinder|Dagvlinder|
|16|S06000042|Lycaena dispar|Grote vuurvlinder|Dagvlinder|
|17|S06000066|Apatura iris|Grote weerschijnvlinder|Dagvlinder|
|18|S06000049|Plebeius argus|Heideblauwtje|Dagvlinder|
|19|S06000106|Hipparchia semele|Heivlinder|Dagvlinder|
|20|S06000098|Coenonympha pamphilus|Hooibeestje|Dagvlinder|
|21|S06000107|Hipparchia statilinus|Kleine heivlinder|Dagvlinder|
|22|S06000064|Limenitis camilla|Kleine ijsvogelvlinder|Dagvlinder|
|23|S06000081|Issoria lathonia|Kleine parelmoervlinder|Dagvlinder|
|24|S06000006|Hesperia comma|Kommavlinder|Dagvlinder|
|25|S06000019|Anthocharis cardamines|Koninginnenpage|Dagvlinder|
|26|S06000061|Maculinea teleius|Pimpernelblauwtje|Dagvlinder|
|27|S06000080|Brenthis ino|Purperstreepparelmoervlinder|Dagvlinder|
|28|S06000097|Coenonympha arcania|Tweekleurig hooibeestje|Dagvlinder|
|29|S06000050|Plebeius idas ssp. idas|Vals heideblauwtje|Dagvlinder|
|30|S06000051|Plebeius optilete|Veenbesblauwtje|Dagvlinder|
|31|S06000076|Boloria aquilonaris|Veenbesparelmoervlinder|Dagvlinder|
|32|S06000099|Coenonympha tullia ssp. tullia|Veenhooibeestje|Dagvlinder|
|33|S06000088|Melitaea cinxia|Veldparelmoervlinder|Dagvlinder|
|34|S06000077|Boloria selene|Zilveren maan|Dagvlinder|
|35|S06000004|Thymelicus lineola|Zwartsprietdikkopje|Dagvlinder|
|36|S09001011|Potentilla sterilis|Aardbeiganzerik|Vaatplant|
|37|S09000330|Cirsium acaule|Aarddistel|Vaatplant|
|38|S09000169|Bunium bulbocastanum|Aardkastanje|Vaatplant|
|39|S09001243|Stachys arvensis|Akkerandoorn|Vaatplant|
|40|S09001042|Ranunculus arvensis|Akkerboterbloem|Vaatplant|
|41|S09000072|Misopates orontium|Akkerleeuwenbek|Vaatplant|
|42|S09000327|Circaea alpina|Alpenheksenkruid|Vaatplant|
|43|S09000491|Euphorbia amygdaloides|Amandelwolfsmelk|Vaatplant|
|44|S09000438|Eleocharis quinqueflora|Armbloemige waterbies|Vaatplant|
|45|S09000603|Helictotrichon pratense|Beemdhaver|Vaatplant|
|46|S09000692|Knautia arvensis|Beemdkroon|Vaatplant|
|47|S09000858|Narthecium ossifragum|Beenbreek|Vaatplant|
|48|S09000157|Bromopsis erecta|Bergdravik|Vaatplant|
|49|S09000951|Platanthera montana|Bergnachtorchis|Vaatplant|
|50|S09000575|Geranium pyrenaicum|Bermooievaarsbek|Vaatplant|
|51|S09000377|Cucubalus baccifer|Besanjelier|Vaatplant|
|52|S09001244|Stachys officinalis|Betonie|Vaatplant|
|53|S09000153|Briza media|Bevertjes|Vaatplant|
|54|S09000444|Elytrigia juncea subsp. boreoatlantica|Biestarwegras|Vaatplant|
|55|S09000642|Hyoscyamus niger|Bilzekruid|Vaatplant|
|56|S09000902|Orobanche picridis|Bitterkruidbremraap|Vaatplant|
|57|S09000389|Cystopteris fragilis|Blaasvaren|Vaatplant|
|58|S09001024|Puccinellia fasciculata|Blauw kweldergras|Vaatplant|
|59|S09001198|Sherardia arvensis|Blauw walstro|Vaatplant|
|60|S09000903|Orobanche purpurea|Blauwe bremraap|Vaatplant|
|61|S09001258|Succisa pratensis|Blauwe knoop|Vaatplant|
|62|S09000486|Eryngium maritimum|Blauwe zeedistel|Vaatplant|
|63|S09000289|Cephalanthera damasonium|Bleek bosvogeltje|Vaatplant|
|64|S09000044|Alyssum alyssoides|Bleek schildzaad|Vaatplant|
|65|S09000541|Galeopsis segetum|Bleekgele hennepnetel|Vaatplant|
|66|S09000247|Carex pallescens|Bleke zegge|Vaatplant|
|67|S09000236|Carex hostiana|Blonde zegge|Vaatplant|
|68|S09001302|Trifolium medium|Bochtige klaver|Vaatplant|
|69|S09000015|Agrostemma githago|Bolderik|Vaatplant|
|70|S09000471|Equisetum variegatum|Bonte paardenstaart|Vaatplant|
|71|S09000857|Nardus stricta|Borstelgras|Vaatplant|
|72|S09001143|Clinopodium vulgare|Borstelkrans|Vaatplant|
|73|S09000529|Fragaria vesca|Bosaardbei|Vaatplant|
|74|S09000588|Gnaphalium sylvaticum|Bosdroogbloem|Vaatplant|
|75|S09001354|Veronica montana|Bosereprijs|Vaatplant|
|76|S09001253|Stellaria nemorum|Bosmuur|Vaatplant|
|77|S09000781|Lysimachia nemorum|Boswederik|Vaatplant|
|78|S09001415|Polypodium interjectum|Brede eikvaren|Vaatplant|
|79|S09001364|Veronica austriaca subsp. teucrium|Brede ereprijs|Vaatplant|
|80|S09000886|Dactylorhiza majalis subsp. majalis|Brede orchis|Vaatplant|
|81|S09000695|Koeleria pyramidata|Breed fakkelgras|Vaatplant|
|82|S09000478|Eriophorum latifolium|Breed wollegras|Vaatplant|
|83|S09001069|Rhynchospora fusca|Bruine snavelbies|Vaatplant|
|84|S09000643|Hypericum canadense|Canadees hertshooi|Vaatplant|
|85|S09000008|Actaea spicata|Christoffelkruid|Vaatplant|
|86|S09000492|Euphorbia cyparissias|Cipreswolfsmelk|Vaatplant|
|87|S09000034|Allium ursinum|Daslook|Vaatplant|
|88|S09000590|Goodyera repens|Dennenorchis|Vaatplant|
|89|S09001356|Veronica opaca|Doffe ereprijs|Vaatplant|
|90|S09000663|Inula conyzae|Donderkruid|Vaatplant|
|91|S09001386|Viola reichenbachiana|Donkersporig bosviooltje|Vaatplant|
|92|S09000324|Cicendia filiformis|Draadgentiaan|Vaatplant|
|93|S09001303|Trifolium micranthum|Draadklaver|Vaatplant|
|94|S09000681|Juncus filiformis|Draadrus|Vaatplant|
|95|S09000239|Carex lasiocarpa|Draadzegge|Vaatplant|
|96|S09000164|Bromus secalinus|Dreps|Vaatplant|
|97|S09000269|Carlina vulgaris|Driedistel|Vaatplant|
|98|S09001162|Schoenoplectus triqueter|Driekantige bies|Vaatplant|
|99|S09000266|Carex trinervis|Drienervige zegge|Vaatplant|
|100|S09001228|Sparganium angustifolium|Drijvende egelskop|Vaatplant|
|101|S09000765|Luronium natans|Drijvende waterweegbree|Vaatplant|
|102|S09000937|Picris echioides|Dubbelkelk|Vaatplant|
|103|S09000146|Blechnum spicant|Dubbelloof|Vaatplant|
|104|S09001147|Scabiosa columbaria|Duifkruid|Vaatplant|
|105|S09000672|Juncus alpinoarticulatus subsp. atricapillus|Duinrus|Vaatplant|
|106|S09000567|Gentianella germanica|Duitse gentiaan|Vaatplant|
|107|S09000917|Parapholis strigosa|Dunstaart|Vaatplant|
|108|S09000288|Centunculus minimus|Dwergbloem|Vaatplant|
|109|S09000686|Juncus pygmaeus|Dwergrus|Vaatplant|
|110|S09000524|Filago minima|Dwergviltkruid|Vaatplant|
|111|S09001038|Radiola linoides|Dwergvlas|Vaatplant|
|112|S09000343|Cochlearia officinalis subsp. officinalis|Echt lepelblad|Vaatplant|
|113|S09001222|Solidago virgaurea|Echte guldenroede|Vaatplant|
|114|S09000479|Eriophorum vaginatum|Eenarig wollegras|Vaatplant|
|115|S09000920|Paris quadrifolia|Eenbes|Vaatplant|
|116|S09000808|Melica uniflora|Eenbloemig parelgras|Vaatplant|
|117|S09000744|Kickxia spuria|Eironde leeuwenbek|Vaatplant|
|118|S09000091|Armeria maritima|Engels gras|Vaatplant|
|119|S09000341|Cochlearia officinalis subsp. anglica|Engels lepelblad|Vaatplant|
|120|S09000170|Bupleurum tenuissimum|Fijn goudscherm|Vaatplant|
|121|S09000569|Geranium columbinum|Fijne ooievaarsbek|Vaatplant|
|122|S09000650|Hypericum pulchrum|Fraai hertshooi|Vaatplant|
|123|S09000565|Gentianella ciliata|Franjegentiaan|Vaatplant|
|124|S09000337|Cladium mariscus|Galigaan|Vaatplant|
|125|S09001319|Ulex europaeus|Gaspeldoorn|Vaatplant|
|126|S09001923|Helianthemum nummularium|Geel zonneroosje|Vaatplant|
|127|S09000747|Linum catharticum|Geelhartje|Vaatplant|
|128|S09000321|Glebionis segetum|Gele ganzenbloem|Vaatplant|
|129|S09000233|Carex flava|Gele zegge|Vaatplant|
|130|S09000148|Botrychium lunaria|Gelobde maanvaren|Vaatplant|
|131|S09000124|Atriplex laciniata|Gelobde melde|Vaatplant|
|132|S09001473|Festuca ovina|Genaald schapengras|Vaatplant|
|133|S09000432|Elatine hexandra|Gesteeld glaskroos|Vaatplant|
|134|S09000595|Atriplex pedunculata|Gesteelde zoutmelde|Vaatplant|
|135|S09001308|Trifolium striatum|Gestreepte klaver|Vaatplant|
|136|S09001335|Valerianella dentata|Getande veldsla|Vaatplant|
|137|S09001313|Tuberaria guttata|Gevlekt zonneroosje|Vaatplant|
|138|S09001616|Dactylorhiza maculata|Gevlekte orchis|Vaatplant|
|139|S09000013|Agrimonia eupatoria|Gewone agrimonie|Vaatplant|
|140|S09000963|Polygala vulgaris|Gewone vleugeltjesbloem|Vaatplant|
|141|S09000652|Hypochaeris glabra|Glad biggenkruid|Vaatplant|
|142|S09001312|Trisetum flavescens|Goudhaver|Vaatplant|
|143|S09000713|Lathyrus nissolia|Graslathyrus|Vaatplant|
|144|S09000748|Liparis loeselii|Groenknolorchis|Vaatplant|
|145|S09000659|Illecebrum verticillatum|Grondster|Vaatplant|
|146|S09000721|Legousia speculum-veneris|Groot spiegelklokje|Vaatplant|
|147|S09000284|Centaurea scabiosa|Grote centaurie|Vaatplant|
|148|S09000750|Neottia ovata|Grote keverorchis|Vaatplant|
|149|S09000074|Aphanes arvensis|Grote leeuwenklauw|Vaatplant|
|150|S09000593|Gymnadenia conopsea|Grote muggenorchis|Vaatplant|
|151|S09001283|Thymus pulegioides|Grote tijm|Vaatplant|
|152|S09000771|Luzula sylvatica|Grote veldbies|Vaatplant|
|153|S09000775|Lycopodium clavatum|Grote wolfsklauw|Vaatplant|
|154|S09001015|Primula veris|Gulden sleutelbloem|Vaatplant|
|155|S09001365|Veronica triphyllos|Handjesereprijs|Vaatplant|
|156|S09000889|Orchis morio|Harlekijn|Vaatplant|
|157|S09000633|Holosteum umbellatum|Heelbeen|Vaatplant|
|158|S09001138|Sanicula europaea|Heelkruid|Vaatplant|
|159|S09000043|Althaea officinalis|Heemst|Vaatplant|
|160|S09000924|Pedicularis sylvatica|Heidekartelblad|Vaatplant|
|161|S09000230|Carex ericetorum|Heidezegge|Vaatplant|
|162|S09001240|Spiranthes spiralis|Herfstschroeforchis|Vaatplant|
|163|S09000345|Colchicum autumnale|Herfsttijloos|Vaatplant|
|164|S09000051|Anacamptis pyramidalis|Hondskruid|Vaatplant|
|165|S09001380|Viola canina|Hondsviooltje|Vaatplant|
|166|S09000608|Herminium monorchis|Honingorchis|Vaatplant|
|167|S09000691|Juniperus communis|Jeneverbes|Vaatplant|
|168|S09001454|Alchemilla glabra|Kale vrouwenmantel|Vaatplant|
|169|S09000553|Galium pumilum|Kalkwalstro|Vaatplant|
|170|S09000386|Cynosurus cristatus|Kamgras|Vaatplant|
|171|S09000271|Carum carvi|Karwij|Vaatplant|
|172|S09001182|Selinum carvifolia|Karwijselie|Vaatplant|
|173|S09000928|Peucedanum carvifolia|Karwijvarkenskervel|Vaatplant|
|174|S09000877|Ononis repens subsp. spinosa|Kattendoorn|Vaatplant|
|175|S09000901|Orobanche minor|Klavervreter|Vaatplant|
|176|S09001324|Utricularia minor|Klein blaasjeskruid|Vaatplant|
|177|S09001174|Scutellaria minor|Klein glidkruid|Vaatplant|
|178|S09001232|Spartina maritima|Klein slijkgras|Vaatplant|
|179|S09000379|Cuscuta epithymum|Klein warkruid|Vaatplant|
|180|S09001033|Pyrola minor|Klein wintergroen|Vaatplant|
|181|S09000668|Isoetes echinospora|Kleine biesvaren|Vaatplant|
|182|S09000749|Neottia cordata|Kleine keverorchis|Vaatplant|
|183|S09000234|Carex hartmanii|Kleine knotszegge|Vaatplant|
|184|S09001067|Rhinanthus minor|Kleine ratelaar|Vaatplant|
|185|S09001953|Thalictrum minus|Kleine ruit|Vaatplant|
|186|S09000800|Medicago minima|Kleine rupsklaver|Vaatplant|
|187|S09001166|Scorzonera humilis|Kleine schorseneer|Vaatplant|
|188|S09001141|Clinopodium acinos|Kleine steentijm|Vaatplant|
|189|S09001284|Thymus serpyllum|Kleine tijm|Vaatplant|
|190|S09001332|Valeriana dioica|Kleine valeriaan|Vaatplant|
|191|S09000913|Vaccinium oxycoccos|Kleine veenbes|Vaatplant|
|192|S09000776|Lycopodium tristachyum|Kleine wolfsklauw|Vaatplant|
|193|S09000494|Euphorbia exigua|Kleine wolfsmelk|Vaatplant|
|194|S09000417|Drosera intermedia|Kleine zonnedauw|Vaatplant|
|195|S09001230|Sparganium natans|Kleinste egelskop|Vaatplant|
|196|S09000481|Erodium lebelii|Kleverige reigersbek|Vaatplant|
|197|S09000568|Gentiana pneumonanthe|Klokjesgentiaan|Vaatplant|
|198|S09000191|Campanula glomerata|Kluwenklokje|Vaatplant|
|199|S09000578|Geum rivale|Knikkend nagelkruid|Vaatplant|
|200|S09000711|Lathyrus linifolius|Knollathyrus|Vaatplant|
|201|S09001144|Saxifraga granulata|Knolsteenbreek|Vaatplant|
|202|S09000039|Alopecurus bulbosus|Knolvossenstaart|Vaatplant|
|203|S09001150|Schoenus nigricans|Knopbies|Vaatplant|
|204|S09001290|Torilis nodosa|Knopig doornzaad|Vaatplant|
|205|S09000217|Carex buxbaumii|Knotszegge|Vaatplant|
|206|S09000279|Centaurea cyanus|Korenbloem|Vaatplant|
|207|S09000094|Arnoseris minima|Korensla|Vaatplant|
|208|S09001130|Salvia verticillata|Kranssalie|Vaatplant|
|209|S09000966|Polygonatum verticillatum|Kranssalomonszegel|Vaatplant|
|210|S09000560|Genista pilosa|Kruipbrem|Vaatplant|
|211|S09000430|Baldellia ranunculoides subsp. repens|Kruipende moerasweegbree|Vaatplant|
|212|S09000566|Gentiana cruciata|Kruisbladgentiaan|Vaatplant|
|213|S09000548|Cruciata laevipes|Kruisbladwalstro|Vaatplant|
|214|S09000961|Polygala comosa|Kuifvleugeltjesbloem|Vaatplant|
|215|S09000231|Carex extensa|Kwelderzegge|Vaatplant|
|216|S09000275|Catapodium marinum|Laksteeltje|Vaatplant|
|217|S09000738|Limonium vulgare|Lamsoor|Vaatplant|
|218|S09000416|Drosera longifolia|Lange zonnedauw|Vaatplant|
|219|S09000055|Andromeda polifolia|Lavendelhei|Vaatplant|
|220|S09000105|Asparagus officinalis subsp. prostratus|Liggende asperge|Vaatplant|
|221|S09001361|Veronica prostrata|Liggende ereprijs|Vaatplant|
|222|S09000962|Polygala serpyllifolia|Liggende vleugeltjesbloem|Vaatplant|
|223|S09000887|Orchis mascula|Mannetjesorchis|Vaatplant|
|224|S09001389|Viola persicifolia|Melkviooltje|Vaatplant|
|225|S09000092|Armoracia rusticana|Mierik|Vaatplant|
|226|S09000456|Epilobium palustre|Moerasbasterdwederik|Vaatplant|
|227|S09000644|Hypericum elodes|Moerashertshooi|Vaatplant|
|228|S09000923|Pedicularis palustris|Moeraskartelblad|Vaatplant|
|229|S09001265|Taraxacum palustre|Moeraspaardenbloem|Vaatplant|
|230|S09000399|Deschampsia setacea|Moerassmele|Vaatplant|
|231|S09000373|Crepis paludosa|Moerasstreepzaad|Vaatplant|
|232|S09000461|Epipactis palustris|Moeraswespenorchis|Vaatplant|
|233|S09000777|Lycopodiella inundata|Moeraswolfsklauw|Vaatplant|
|234|S09000496|Euphorbia palustris|Moeraswolfsmelk|Vaatplant|
|235|S09000031|Allium oleraceum|Moeslook|Vaatplant|
|236|S09002417|Hieracium murorum|Muurhavikskruid|Vaatplant|
|237|S09000710|Lathyrus aphaca|Naakte lathyrus|Vaatplant|
|238|S09001148|Scandix pecten-veneris|Naaldenkervel|Vaatplant|
|239|S09000806|Silene noctiflora|Nachtkoekoeksbloem|Vaatplant|
|240|S09000214|Carex aquatilis|Noordse zegge|Vaatplant|
|241|S09000753|Littorella uniflora|Oeverkruid|Vaatplant|
|242|S09001309|Trifolium subterraneum|Onderaardse klaver|Vaatplant|
|243|S09000077|Apium inundatum|Ondergedoken moerasscherm|Vaatplant|
|244|S09000993|Potamogeton gramineus|Ongelijkbladig fonteinkruid|Vaatplant|
|245|S09001205|Silene otites|Oorsilene|Vaatplant|
|246|S09001263|Taraxacum obliquum|Oranjegele paardenbloem|Vaatplant|
|247|S09001164|Scleranthus perennis|Overblijvende hardbloem|Vaatplant|
|248|S09000323|Chrysosplenium oppositifolium|Paarbladig goudveil|Vaatplant|
|249|S09000213|Carex appropinquata|Paardenhaarzegge|Vaatplant|
|250|S09000921|Parnassia palustris|Parnassia|Vaatplant|
|251|S09000939|Pilularia globulifera|Pilvaren|Vaatplant|
|252|S09001323|Utricularia intermedia|Plat blaasjeskruid|Vaatplant|
|253|S09001157|Blysmus compressus|Platte bies|Vaatplant|
|254|S09000817|Mentha pulegium|Polei|Vaatplant|
|255|S09000891|Orchis purpurea|Purperorchis|Vaatplant|
|256|S09000196|Campanula rapunculus|Rapunzelklokje|Vaatplant|
|257|S09000360|Corrigiola litoralis|Riempjes|Vaatplant|
|258|S09001330|Vaccinium uliginosum|Rijsbes|Vaatplant|
|259|S09000719|Leersia oryzoides|Rijstgras|Vaatplant|
|260|S09001158|Blysmus rufus|Rode bies|Vaatplant|
|261|S09000472|Erica cinerea|Rode dophei|Vaatplant|
|262|S09000509|Odontites vernus subsp. serotinus|Rode ogentroost|Vaatplant|
|263|S09001034|Pyrola rotundifolia|Rond wintergroen|Vaatplant|
|264|S09000221|Carex diandra|Ronde zegge|Vaatplant|
|265|S09000418|Drosera rotundifolia|Ronde zonnedauw|Vaatplant|
|266|S09000392|Daphne mezereum|Rood peperboompje|Vaatplant|
|267|S09000061|Antennaria dioica|Rozenkransje|Vaatplant|
|268|S09000645|Hypericum hirsutum|Ruig hertshooi|Vaatplant|
|269|S09000402|Dianthus armeria|Ruige anjer|Vaatplant|
|270|S09000726|Leontodon hispidus|Ruige leeuwentand|Vaatplant|
|271|S09000949|Plantago media|Ruige weegbree|Vaatplant|
|272|S09000827|Milium vernale|Ruw gierstgras|Vaatplant|
|273|S09000751|Lithospermum arvense|Ruw parelzaad|Vaatplant|
|274|S09000536|Gagea spathacea|Schedegeelster|Vaatplant|
|275|S09000293|Cerastium diffusum|Scheve hoornbloem|Vaatplant|
|276|S09001262|Taraxacum celticum|Schraallandpaardenbloem|Vaatplant|
|277|S09000076|Apium graveolens|Selderij|Vaatplant|
|278|S09001111|Sagina nodosa|Sierlijke vetmuur|Vaatplant|
|279|S09000477|Eriophorum gracile|Slank wollegras|Vaatplant|
|280|S09000562|Gentianella amarella|Slanke gentiaan|Vaatplant|
|281|S09000242|Carex limosa|Slijkzegge|Vaatplant|
|282|S09000067|Anthoxanthum aristatum|Slofhak|Vaatplant|
|283|S09001107|Ruppia maritima|Snavelruppia|Vaatplant|
|284|S09000888|Orchis militaris|Soldaatje|Vaatplant|
|285|S09000332|Cirsium dissectum|Spaanse ruiter|Vaatplant|
|286|S09000742|Kickxia elatine|Spiesleeuwenbek|Vaatplant|
|287|S09001460|Caltha palustris subsp. araneosa|Spindotterbloem|Vaatplant|
|288|S09000612|Hieracium lactucella|Spits havikskruid|Vaatplant|
|289|S09000404|Dianthus deltoides|Steenanjer|Vaatplant|
|290|S09000558|Genista anglica|Stekelbrem|Vaatplant|
|291|S09000774|Lycopodium annotinum|Stekende wolfsklauw|Vaatplant|
|292|S09001016|Primula vulgaris|Stengelloze sleutelbloem|Vaatplant|
|293|S09000276|Catapodium rigidum|Stijf hardgras|Vaatplant|
|294|S09000175|Calamagrostis stricta|Stijf struisriet|Vaatplant|
|295|S09000845|Myosotis stricta|Stijf vergeet-mij-nietje|Vaatplant|
|296|S09000429|Baldellia ranunculoides subsp. ranunculoides|Stijve moerasweegbree|Vaatplant|
|297|S09000979|Polystichum aculeatum|Stijve naaldvaren|Vaatplant|
|298|S09002316|Euphrasia stricta s.l.|Stijve ogentroost|Vaatplant|
|299|S09000488|Erysimum virgatum|Stijve steenraket|Vaatplant|
|300|S09000063|Anthemis cotula|Stinkende kamille|Vaatplant|
|301|S09000256|Carex punctata|Stippelzegge|Vaatplant|
|302|S09000834|Monotropa hypopitys|Stofzaad|Vaatplant|
|303|S09000138|Beta vulgaris subsp. maritima|Strandbiet|Vaatplant|
|304|S09000053|Anagallis tenella|Teer guichelheil|Vaatplant|
|305|S09000850|Myriophyllum alterniflorum|Teer vederkruid|Vaatplant|
|306|S09000829|Minuartia hybrida|Tengere veldmuur|Vaatplant|
|307|S09001315|Arabis glabra|Torenkruid|Vaatplant|
|308|S09001180|Sedum rupestre|Tripmadam|Vaatplant|
|309|S09001610|Bromus racemosus|Trosdravik|Vaatplant|
|310|S09000223|Carex dioica|Tweehuizige zegge|Vaatplant|
|311|S09000093|Arnica montana|Valkruid|Vaatplant|
|312|S09000062|Anthemis arvensis|Valse kamille|Vaatplant|
|313|S09001149|Scheuchzeria palustris|Veenbloembies|Vaatplant|
|314|S09000597|Hammarbya paludosa|Veenmosorchis|Vaatplant|
|315|S09000626|Hierochloe odorata|Veenreukgras|Vaatplant|
|316|S09000563|Gentianella campestris|Veldgentiaan|Vaatplant|
|317|S09000637|Hordeum secalinum|Veldgerst|Vaatplant|
|318|S09001128|Salvia pratensis|Veldsalie|Vaatplant|
|319|S09000561|Genista tinctoria|Verfbrem|Vaatplant|
|320|S09000467|Equisetum ramosissimum|Vertakte paardenstaart|Vaatplant|
|321|S09000942|Pinguicula vulgaris|Vetblad|Vaatplant|
|322|S09000222|Carex digitata|Vingerzegge|Vaatplant|
|323|S09000884|Dactylorhiza incarnata|Vleeskleurige orchis|Vaatplant|
|324|S09000881|Ophrys insectifera|Vliegenorchis|Vaatplant|
|325|S09001154|Eleogiton fluitans|Vlottende bies|Vaatplant|
|326|S09000255|Carex pulicaris|Vlozegge|Vaatplant|
|327|S09000861|Neottia nidus-avis|Vogelnestje|Vaatplant|
|328|S09001304|Trifolium ornithopodioides|Vogelpootklaver|Vaatplant|
|329|S09001013|Potentilla tabernaemontani|Voorjaarsganzerik|Vaatplant|
|330|S09000218|Carex caryophyllea|Voorjaarszegge|Vaatplant|
|331|S09000268|Carex vulpina|Voszegge|Vaatplant|
|332|S09000346|Comarum palustre|Wateraardbei|Vaatplant|
|333|S09000821|Menyanthes trifoliata|Waterdrieblad|Vaatplant|
|334|S09001183|Jacobaea aquatica|Waterkruiskruid|Vaatplant|
|335|S09000764|Ludwigia palustris|Waterlepeltje|Vaatplant|
|336|S09000754|Lobelia dortmanna|Waterlobelia|Vaatplant|
|337|S09001200|Silaum silaus|Weidekervel|Vaatplant|
|338|S09000014|Agrimonia procera|Welriekende agrimonie|Vaatplant|
|339|S09000950|Platanthera bifolia|Welriekende nachtorchis|Vaatplant|
|340|S09000689|Juncus tenageia|Wijdbloeiende rus|Vaatplant|
|341|S09000862|Nepeta cataria|Wild kattenkruid|Vaatplant|
|342|S09000849|Myrica gale|Wilde gagel|Vaatplant|
|343|S09000532|Fritillaria meleagris|Wilde kievitsbloem|Vaatplant|
|344|S09000818|Mentha suaveolens|Witte munt|Vaatplant|
|345|S09000936|Phyteuma spicatum subsp. spicatum|Witte rapunzel|Vaatplant|
|346|S09001068|Rhynchospora alba|Witte snavelbies|Vaatplant|
|347|S09001053|Ranunculus ololeucos|Witte waterranonkel|Vaatplant|
|348|S09000071|Anthyllis vulneraria|Wondklaver|Vaatplant|
|349|S09001193|Serratula tinctoria|Zaagblad|Vaatplant|
|350|S09001181|Sedum sexangulare|Zacht vetkruid|Vaatplant|
|351|S09000500|Euphorbia seguieriana|Zandwolfsmelk|Vaatplant|
|352|S09000100|Seriphidium maritimum|Zeealsem|Vaatplant|
|353|S09000635|Hordeum marinum|Zeegerst|Vaatplant|
|354|S09001110|Sagina maritima|Zeevetmuur|Vaatplant|
|355|S09000948|Plantago maritima|Zeeweegbree|Vaatplant|
|356|S09000497|Euphorbia paralias|Zeewolfsmelk|Vaatplant|
|357|S09000870|Oenanthe lachenalii|Zilt torkruid|Vaatplant|
|358|S09001379|Viola lutea subsp. calaminaria|Zinkviooltje|Vaatplant|
|359|S09000734|Leucojum aestivum|Zomerklokje|Vaatplant|
|360|S09000935|Phyteuma spicatum subsp. nigrum|Zwartblauwe rapunzel|Vaatplant|
|361|S09000356|Cornus suecica|Zweedse kornoelje|Vaatplant|
|362|S02017170|Coccothraustes coccothraustes|Appelvink|Broedvogel|
|363|S02013640|Panurus biarmicus|Baardman|Broedvogel|
|364|S02001730|Tadorna tadorna|Bergeend|Broedvogel|
|365|S02011060|Luscinia svecica|Blauwborst|Broedvogel|
|366|S02002610|Circus cyaneus|Blauwe kiekendief|Broedvogel|
|367|S02004700|Charadrius hiaticula|Bontbekplevier|Broedvogel|
|368|S02013490|Ficedula hypoleuca ssp. hypoleuca|Bonte vliegenvanger|Broedvogel|
|369|S02014790|Sitta europaea|Boomklever|Broedvogel|
|370|S02014870|Certhia brachydactyla|Boomkruiper|Broedvogel|
|371|S02009740|Lullula arborea|Boomleeuwerik|Broedvogel|
|372|S02010090|Anthus trivialis|Boompieper|Broedvogel|
|373|S02012740|Sylvia curruca|Braamsluiper|Broedvogel|
|374|S02002600|Circus aeruginosus|Bruine kiekendief|Broedvogel|
|375|S02014900|Remiz pendulinus|Buidelmees|Broedvogel|
|376|S02002870|Buteo buteo ssp. buteo|Buizerd|Broedvogel|
|377|S02012200|Cettia cetti|Cetti's zanger|Broedvogel|
|378|S02000070|Tachybaptus ruficollis|Dodaars|Broedvogel|
|379|S02008480|Jynx torquilla|Draaihals|Broedvogel|
|380|S02010050|Anthus campestris|Duinpieper|Broedvogel|
|381|S02006240|Sterna albifrons|Dwergstern|Broedvogel|
|382|S02002060|Somateria mollissima|Eider|Broedvogel|
|383|S02013080|Phylloscopus sibilatrix|Fluiter|Broedvogel|
|384|S02018570|Emberiza citrinella|Geelgors|Broedvogel|
|385|S02011220|Phoenicurus phoenicurus|Gekraagde roodstaart|Broedvogel|
|386|S02010171|Motacilla flava|Gele kwikstaart|Broedvogel|
|387|S02000120|Podiceps nigricollis|Geoorde fuut|Broedvogel|
|388|S02014400|Parus palustris ssp. palustris|Glanskop|Broedvogel|
|389|S02010110|Anthus pratensis|Graspieper|Broedvogel|
|390|S02018820|Emberiza calandra|Grauwe gors|Broedvogel|
|391|S02002630|Circus pygargus|Grauwe kiekendief|Broedvogel|
|392|S02015150|Lanius collurio|Grauwe klauwier|Broedvogel|
|393|S02013350|Muscicapa striata|Grauwe vliegenvanger|Broedvogel|
|394|S02004590|Burhinus oedicnemus ssp. oedicnemus|Griel|Broedvogel|
|395|S02008560|Picus viridis|Groene specht|Broedvogel|
|396|S02016490|Chloris chloris|Groenling|Broedvogel|
|397|S02008760|Dendrocopos major|Grote bonte specht|Broedvogel|
|398|S02010190|Motacilla cinerea ssp. cinerea|Grote gele kwikstaart|Broedvogel|
|399|S02012530|Acrocephalus arundinaceus|Grote karekiet|Broedvogel|
|400|S02006110|Sterna sandvicensis|Grote stern|Broedvogel|
|401|S02001210|Casmerodius albus|Grote zilverreiger|Broedvogel|
|402|S02005320|Limosa limosa|Grutto|Broedvogel|
|403|S02002670|Accipiter gentilis ssp. gentilis|Havik|Broedvogel|
|404|S02008310|Alcedo atthis ssp. ispida|IJsvogel|Broedvogel|
|405|S02016380|Fringilla montifringilla|Keep|Broedvogel|
|406|S02005170|Philomachus pugnax|Kemphaan|Broedvogel|
|407|S02015200|Lanius excubitor|Klapekster|Broedvogel|
|408|S02004100|Porzana parva|Klein waterhoen|Broedvogel|
|409|S02016634|Carduelis cabaret|Kleine barmsijs|Broedvogel|
|410|S02008870|Dendrocopos minor|Kleine bonte specht|Broedvogel|
|411|S02004690|Charadrius dubius|Kleine plevier|Broedvogel|
|412|S02004110|Porzana pusilla|Kleinst waterhoen|Broedvogel|
|413|S02004560|Recurvirostra avosetta|Kluut|Broedvogel|
|414|S02016600|Carduelis cannabina|Kneu|Broedvogel|
|415|S02003320|Tetrao tetrix|Korhoen|Broedvogel|
|416|S02004330|Grus grus|Kraanvogel|Broedvogel|
|417|S02001820|Anas strepera|Krakeend|Broedvogel|
|418|S02001960|Netta rufina|Krooneend|Broedvogel|
|419|S02002030|Aythya fuligula|Kuifeend|Broedvogel|
|420|S02001040|Nycticorax nycticorax|Kwak|Broedvogel|
|421|S02003700|Coturnix coturnix|Kwartel|Broedvogel|
|422|S02004210|Crex crex|Kwartelkoning|Broedvogel|
|423|S02001440|Platalea leucorodia|Lepelaar|Broedvogel|
|424|S02014420|Parus montanus|Matkop|Broedvogel|
|425|S02008830|Dendrocopos medius|Middelste bonte specht|Broedvogel|
|426|S02017100|Pyrrhula pyrrhula ssp. europoea|Midden-Europese goudvink|Broedvogel|
|427|S02011040|Luscinia megarhynchos|Nachtegaal|Broedvogel|
|428|S02007780|Caprimulgus europaeus ssp. europaeus|Nachtzwaluw|Broedvogel|
|429|S02006160|Sterna paradisaea|Noordse stern|Broedvogel|
|430|S02009810|Riparia riparia ssp. riparia|Oeverzwaluw|Broedvogel|
|431|S02001340|Ciconia ciconia ssp. ciconia|Ooievaar|Broedvogel|
|432|S02018660|Emberiza hortulana|Ortolaan|Broedvogel|
|433|S02011370|Saxicola rubetra|Paapje|Broedvogel|
|434|S02003670|Perdix perdix|Patrijs|Broedvogel|
|435|S02001890|Anas acuta ssp. acuta|Pijlstaart|Broedvogel|
|436|S02004080|Porzana porzana|Porseleinhoen|Broedvogel|
|437|S02001240|Ardea purpurea|Purperreiger|Broedvogel|
|438|S02016530|Carduelis carduelis|Putter|Broedvogel|
|439|S02015720|Corvus corax|Raaf|Broedvogel|
|440|S02012430|Acrocephalus schoenobaenus|Rietzanger|Broedvogel|
|441|S02000950|Botaurus stellaris|Roerdomp|Broedvogel|
|442|S02011390|Saxicola rubicola|Roodborsttapuit|Broedvogel|
|443|S02004500|Haematopus ostralegus|Scholekster|Broedvogel|
|444|S02016540|Carduelis spinus|Sijs|Broedvogel|
|445|S02001940|Anas clypeata|Slobeend|Broedvogel|
|446|S02012380|Locustella luscinioides|Snor|Broedvogel|
|447|S02012590|Hippolais icterina|Spotvogel|Broedvogel|
|448|S02012360|Locustella naevia|Sprinkhaanzanger|Broedvogel|
|449|S02004770|Charadrius alexandrinus|Strandplevier|Broedvogel|
|450|S02011460|Oenanthe oenanthe|Tapuit|Broedvogel|
|451|S02003040|Falco tinnunculus ssp. tinnunculus|Torenvalk|Broedvogel|
|452|S02005460|Tringa totanus|Tureluur|Broedvogel|
|453|S02009760|Alauda arvensis|Veldleeuwerik|Broedvogel|
|454|S02007680|Asio flammeus|Velduil|Broedvogel|
|455|S02006150|Sterna hirundo|Visdief|Broedvogel|
|456|S02013150|Regulus ignicapilla|Vuurgoudhaan|Broedvogel|
|457|S02004070|Rallus aquaticus|Waterral|Broedvogel|
|458|S02005190|Gallinago gallinago|Watersnip|Broedvogel|
|459|S02002310|Pernis apivorus|Wespendief|Broedvogel|
|460|S02015080|Oriolus oriolus|Wielewaal|Broedvogel|
|461|S02001840|Anas crecca|Wintertaling|Broedvogel|
|462|S02000980|Ixobrychus minutus|Woudaap|Broedvogel|
|463|S02005410|Numenius arquata|Wulp|Broedvogel|
|464|S02012000|Turdus philomelos|Zanglijster|Broedvogel|
|465|S02001910|Anas querquedula|Zomertaling|Broedvogel|
|466|S02008630|Dryocopus martius|Zwarte specht|Broedvogel|
|467|S02006270|Chlidonias niger ssp. niger|Zwarte stern|Broedvogel|
|468|S02005750|Larus melanocephalus|Zwartkopmeeuw|Broedvogel|
   

  